---
title: About me
subtitle: Why you'd want to hang out with me
comments: false
---
<br />
<br />
<br />
<br />
<br />
<br />
Mijn naam is Tyoni Antonio van Roosendaal. Ik hou van:

- Lange strandwandelingen
- gezelschapsspelletjes
- Humor

Ik heb een paar quotes waar ik het leven mee door ga. Deze zijn:

- Never pass up a good handshake.
- Where's the fun if there's no challenge. 

### Mijn [favoriete website](http://www.theuselessweb.com/)
