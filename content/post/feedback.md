---
title: "Peerfeedback"
subtitle:  "Last one"
date:  2018-05-25
categories: ["School","Kwartaal4"]
tags: ["Blog","School"]
---
Het is onze laatste feedback moment van het jaar! Lachen, gieren brullen
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Tijdens dit moment kreeg ik nog wat feedback op mezelf en hoe ik het deed als teamcaptain en heb ik feedback gegeven aan mijn teamleden!.
De volgende 2 dingen waren de "main topics" van de feedback op mij:
<br/>
<br/>
<ul>
<li>Ik moet wat aan verbeelden doen</li>
<br/>
Dit was feedback die van mijn team kwam en ik me ook wel in kon vinden sinds ik probeer meer te verbeelden. Ze gaven me dan ook een tip om de planning wat beter uit te beelden.
<br/>
<br/>
<li>Concentratie/Team concentratie</li>
<br/>
Ook best wel nuttig sinds ik zelf wel moeite heb om me soms bij de opdracht te houden en mijn team heeft het zelfde de probleem dus het was mijn taak om ze erbij te houden.
Soms ging dat wat beter dan andere keren helaas en daar moet ik nog in verbeteren dan.
</ul>
<br/>
<br/>
Om terug te kijken op deze feedback momenten. Ik merk wel dat ze meer open zijn geworden bij deze momenten vergeleken met de eerste keer. Helaas moest ik wel als eerst beginnen om het op te starten maar dat helpt mij ook weer met wat directer zijn/initiatief nemen.
Deze momenten zijn ook veel fijner en persoonlijker dan een formulier gebruiken sinds je soms wat meer kwijt kan in een gesprek dan iets wat je opschrijft.