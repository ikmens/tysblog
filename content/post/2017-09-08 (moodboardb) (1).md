---
title: "What a time to be a Ty"
subtitle:  "first post"
date:  2017-09-08
categories: ["Personal progression","Kwartaal1","School"]
tags: ["Blog"]
---

Hierin laat ik mijn gedachte proces zien die ik tijdens mijn projecten heb.
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
---
>**Moodboards**
>`8 september 2017`
>&nbsp;
>Moodboard over de CMD'er
>![Moodboard over CMD'ers gemaakt door Tyoni](https://68.media.tumblr.com/fb47bd4850d6487441058f30c47ae1f3/tumblr_ovymjcAKB11sj8og3o1_540.png)
>_Afbeelding gevonden op het internet_
>&nbsp;
>In deze moodboard liet ik zien wat ik dacht toen ik over een CMD'er dacht. Aan het begin keek ik om me heen in de studio om te zien wat iedereen gemeen had. Hierdoor zag ik dat de meeste muziek luisterde of iets aan het knutselen/tekenen waren.
>&nbsp;
>&nbsp;
>_Moodboard over het openbaar vervoer_
>![Moodboard OV](https://68.media.tumblr.com/5c11851a3706eb2c59f0200d85cb0c24/tumblr_ovymjcAKB11sj8og3o2_540.png)
>_Afbeelding gemaakt door mij (Tyoni) en door een teamgenoot (Gabi)_
>&nbsp;
>Met deze moodboard liet ik mijn gedachten over het Openbaar vervoer zien. Omdat als ik over het OV denk moet ik denken aan trams, metro's, watertaxi's/bussen, treinen en bussen. De meeste van deze dingen vind je ook bij Rotterdam centraal station.
>&nbsp;
>_(Beide moodboards zullen waarschijnlijk worden verbetered maar zijn nu hier geplaatst om mijn basis van de toekomstige moodboards te bewaren.)_
&nbsp;

---

_Written by Tyoni van Roosendaal_
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; |_with [StackEdit](https://stackedit.io/)._
&nbsp;
_Everything here is subject to change_
