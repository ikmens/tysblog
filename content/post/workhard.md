---
title: "Working hard"
subtitle:  "Na de vakantie"
date:  2017-10-23
categories: ["Personal progression","School","Kwartaal1"]
tags: ["Blog","Project"]
---
Dag na de vakantie en we zijn weer hard aan het werk. Vandaag hebben we het bord geprint en geplakt op een ander bordspel(dankje Savannah!).
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Ook heb ik samen met Gabi het ontwerpproceskaart ingericht van de laastste paar weken.
Nu nog bezig om te kijken wat we kunnen gebruiken bij onze stand bij de expo om op te vallen.
Tot nu toe verzonnen om een whiteboard mee te nemen en ons logo er op te schrijven/tekenen hoewel we die nog niet hebben.
Misschien ook nog ergens karton vandaan halen om zo wat borden te maken.
En een infographic maar dat is voor morgen geplanned!
