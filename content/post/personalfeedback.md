---
title: "Personal feedback"
subtitle:  "Improving myself!"
date:  2017-12-11
categories: ["Personal progression","Kwartaal2"]
tags: ["Blog","School"]
---
<p>Peer feedback formulier gemaakt en rond laten gaan in mijn team. 
Hiermee kan ik er achterkomen waarin ik mezelf kan verbeteren.</p>
<!--more-->

<img src="https://i.imgur.com/Qv9s1bW.jpg" alt="form">
