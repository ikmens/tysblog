---
title: "Finishing up prototype"
subtitle:  "But does it do this!?"
date:  2019-06-17
categories: ["School","Year 2"]
tags: ["SemesterDos"]
---
Laatste paar dingen tot het einde van het schooljaar. Het gaat zo snel en toch voelt het alsof het zo langzaam gaat. Oh right en tekenen....
<!--more-->

<br />
<br />
<br />
<br />
Laten we eerst over vorige week praten. Prototype party!!! Een prototype party is wanneer iedereen bij elkaar zijn prototype gaat proberen, feedback geven en inspiratie op doet voor mogelijke aanpassingen. 
Helaas waren wij maar met ze 2en omdat Ashana ziek was en Sanne haar kuitspier had geschuurd dus nauwelijks kon lopen. Helaas naja we waren tenminste met ze 2en en ik was niet in me eentje. 
Het prototype dat we ervoor zouden gebruiken was helaas vrij onduidelijk dus hadden we besloten om het prototype die daar voor kwam van Hannad. <i>Niet dat we het nieuwe prototype niet hebben getest maar we konden zelf ook zien dat het voor verwarring zou zorgen en deze speculatie werd ook alweer versterkt door mensen die het gingen testen.</i> 
Dus met het oude prototype gingen we testen. Super top was usability feedback gekregen die we ook al hadden verwacht maar ook andere dingen zoals het horizontale filmpje. Ze vonden het mooi en wel passend. De animaties zorgden er ook voor dat het wat meer "levend" was vergeleken met het nieuwe prototype. 
Buiten de testen die wij hebben gedaan was ik ook test persoon! Ik was testpersoon bij wat mensen voor AD. Beetje kijken waar de andere klassen mee bezig waren. Super interessant! 
Sommige mensen waren heel ver in het process en sommige moesten opnieuw beginnen voor een of andere reden. Het was heel leuk om te zien dat de kwaliteit en variatie van prototypes heel anders is dan vorige jaar. 
Sommige mensen hadden wat gebouwd van houd, andere posters of filmpjes en sommige een best wel mooi prototype via XD. 
<br />
<br />
Na de party had ik besloten om het prototype te maken omdat ik toch wel de prototyper was. Het was een hele onderneming maar ik ben nu wel ongeveer klaar. Het duurde wel even om heel de app na te maken en dan ons concept erin te "injecteren". 
Het was moeilijk omdat ons concept niet een heel nieuw ding is. Het is eigenlijk gewoon een klein ding dat de NRC voor je aanpast. Ook wel fijn om iets subtiels te hebben inplaats van flashy. 
Straks ga ik een testplan opstellen om de usability van het prototype te testen om zo nog een beetje te fine tunen voor de expo op 25 juni. Ik moet dit wat eerder doen dan me team omdat ik daarna tekenen pressure cooker heb. Hierin ga ik 3 dagen 3 uur lang werken aan teken opdrachten en word ik formatief beoordeeld op mijn voortgang om dan te zien of ik tekenen heb behaald. Laten we het hopen want tekenen is echt een bottleneck geweest naarmate van school pad en kan me nog meer gaan hinderen als ik erdoor mijn P niet behaal voor stage. 
<br /> 
<br />
Anyhow <b><i>Peace out. Girl Scout!</i></b>
