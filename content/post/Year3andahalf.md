---
title: "Plannen voor het semester"
subtitle:  "Let's do this!!!"
date:  2020-02-17
categories: ["School","Year 3"]
tags: ["Year3, Project"]
---
First day na stage. Vandaag weet ik niet precies wat er gaat gebeuren dus hierin vooral mijn plannen voor dit semester...
<!--more-->

<br />
<br />
<br />
<br />
Dus ja stage zit er weer op. Snel gegaan maar ook echt niet. Ik mis werk wel, de mensen, mijn bureau en de gratis lunch. Maar hé misschien kan ik er gaan werken na me studie. 
<br />
<br />
<h2>Plannen voor dit semester</h2>
<p>
Tijdens dit semester wil ik een aantal dingen doen. Ik wil bijvoorbeeld mijn portfolio verbeteren. Dit keer eentje waar ik echt tevreden ben en die op alles kan worden gebruikt. 
Ik ben er al mee begonnen in html-css en ik probeer het voor alle sizes te maken en ik ben benieuwd hoe lang me dit gaat duren. 
<br />
<br />
Iets anders dat ik wil gaan proberen is Python. Naja niet echt proberen maar leren sinds ik dat nodig ga hebben tijdens mijn minor dus ik heb me ingeschreven voor een keuzevak waarin er mee word gewerkt zodat ik het kan gaan leren. 
En Python lijkt me zowiezo nuttig om te leren omdat het meerdere realworld mogelijkheden heeft!
<br />
<br />
En als laatst wil ik een autonoom project gaan doen. Ik weet nog niet precies wat ik er in wil gaan doen maar ik wil het pas volgend kwartaal gaan doen dus ik heb nog wat denktijd. 
</p>
<br />
<br />
<b><i>Anyhow that's all for now. Peace out. Girl Scout!</i></b>