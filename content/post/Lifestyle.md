---
title: "Lifestyle Diary"
subtitle:  ""
date:  2018-03-06
categories: ["School","Kwartaal3"]
tags: ["Blog","Project","School"]
---
Het is de week na de vakantie! We gaan weer hard aan het werk aan het project. 
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Deze week moeten we onze persoonlijke [Lifestyle Diary](https://docs.google.com/document/d/1nwaB5R2kW5AbaZaeCxSh1VV2prsWxVyVvceogqTodWI/edit) afmaken en deze op vrijdag inleveren.
We zijn dus allemaal begonnen met eraan werken. Zelf ben ik eerst gaan kijken wat ik doe in opzicht van voeding, ontspanning en beweging.
<img src="https://i.imgur.com/5ycw63u.jpg" alt="lifestylewords" width="500" height="600">
