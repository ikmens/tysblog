---
title: "Creatieve techniek"
subtitle:  "A hell of a job"
date:  2018-03-23
categories: ["School","Kwartaal3"]
tags: ["Blog","School"]
---
We hebben een creatieve techniek gebruikt om op twee ideeën te komen waar we uiteindelijk er 1 van hebben gekozen. De techniek die we hebben gebruikt is <i>"Advocaat van de duivel"</i>.
Met deze techniek moet er 1 iemand tegen het idee zijn, 1 iemand voor het idee zijn en een notulist zijn.<br /><br />
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Sinds wij met ze vijfen waren hebben we 2 voor, 2 tegen en 1 notulist/leider gedaan.
Deze notulist/leider was Savannah. Zij heeft alle belangrijke punten opgeschreven en opgenomen met haar telefoon.
Het gehele process duurde best wel lang (~1.5 uur) en werd aan het eind vrij saai. Volgende keer zal ik dus gaan kijken of ik ervoor kan zorgen dat het was sneller gaat en wat interessanter is over tijd.
<br /> <br />
<img src="https://i.imgur.com/XJO3yHx.jpg" alt="lifestylewords" width="500" height="600">
