---
title: "The Floor is Mine!"
subtitle: "Dance!"
date: 2018-05-16
categories: ["Kwartaal4","School"]
tags: ["Blog", "School"]
---
Het is woensdag en ik kom net bij [Het Klooster](https://www.google.nl/search?q=het+klooster+afrikaanderplein&oq=Het+klooster+afri&aqs=chrome.0.0j69i57j0l3.3287j0j7&sourceid=chrome&ie=UTF-8) aan.
Ik was uitgenodigd door Chris, iemand die ik vorige week had geïnterviewd, om te kijken bij één van zijn breakdancelessen die hij geeft aan basisschoolkinderen ([The Floor is Yours](https://www.jeugdcultuurfonds.nl/thefloorisyourss/the-floor-is-yours-rotterdam/)).
Hoewel de kinderen niet in onze doelgroep zitten is het nog steeds belangrijk om te zien of zij al weten hoe je gezond bezig kan zijn. 
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Tijdens mijn bezoek heb ik 2 mensen geïnterviewd en een groepje kinderen die les volgde van Chris. 
Door deze interviews ben ik erachter gekomen dat de mensen van Afrikaanderwijk best wel close met elkaar zijn. 
Deze informatie helpt ons met het maken van extra dingen bij ons concept en geeft ons meer inzicht op Afrikaanderwijk.
Ook de interview met het groepje basisschool kinderen hielp heel veel. Hierdoor kwamen we erachter dat de kinderen wel wisten ze hoe ze zich gezond moesten houden.
Door middel van goed eten en bewegen.