---
title: "Am I in the wrong?"
subtitle:  "Or are they in the wrong?"
date:  2019-09-09
categories: ["School","Year 3"]
tags: ["Year3, Ethic"]
---
Ethics is knowing the difference between what you have a right to do and what is right to do...
<!--more-->

<br />
<br />
<br />
Er is weer een nieuw schooljaar gekomen en helaas heb ik geen stage op dit moment. Dat betekent dat ik gewoon les heb in dat half jaar en dan volgend half jaar een stage ga lopen. 
Dat betekent dat ik nu al begin met leren over ethiek in design en waar ik sta als designer. 
<br />
<br />
Voor de eerste les hadden we het over wat ethiek is, waarom het belangrijk is en hoe het word gebruikt. De reden hiervoor is <i><u>om bewuster naar design te gaan kijken.</u></i> 
En ik snap ook helemaal waarom het belangrijk is. Het zorgt ervoor dat je kan zien waar je later kan gaan werken, aan wat voor dingen je zult gaan werken en hoe deze dingen eruit komen te zien. 
<br />
<br />
Om een voorbeeld te geven van de les....<b>zou jij een app creeëren voor ABN Amro die kinderen helpt leren sparen? Wetende dat ze landmijnen hebben gefinancieerd die zo goed als 100.000 kinderen hebben vermoord in een ander land.</b> 
En ik heb weet het nog niet zo zeker. Het is best wel hevig om voor zo'n bedrijf te werken maar aan de andere kant help je wel weer kinderen hier om later betere volwassenen te zijn. 
Dat was ook de bedoeling van de vraag zo ver ik het begreep. Om ons bewust te maken van onze eigen moreele waardes (of waarden idk). 
<br />
<br />
Na mijn geloof dat ik mijzelf kende te hebben vernietigd zijn we verder gegaan met een opdracht, een paar om precies te zijn. Laat ik ze eerst vertellen en dat hoe ik ze zelf heb uitgewerkt/aan begonnen ben.
<br />
<br />
De opdrachten zijn: <br />
<ol>
    <li>Doe de test op <a href="https://www.123test.nl/persoonlijke-waarden-test/">123tests.nl</a> en koppel deze waardes aan <a href="https://www.google.com/search?q=schwartz+values+full+map&rlz=1C1CHBD_enNL821NL821&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjz_9Pm8MXkAhXIYlAKHcXfBWUQ_AUIEigB&biw=1920&bih=1007#imgrc=VbmReGVL9rRWPM:">Schwartz waardes</a></li>
    <li>Kijk naar je social media gedrag. Hoe laat je jezelf daarop zien? Hoe gebruik je het? Hoe verhoudt dit alles zich tot jouw Schwartz waarden en hoe komen jouw waarden tot uiting in gedrag en imago?</li>
    <li>Visualiseer de uitkomsten van de vorige opdracht</li>
</ol>
<br />
Dus opdracht 1. Ik heb de test gedaan en kwam uit op 2 waardes die ik het belangrijkst vond. Innerlijke groei en vriendschap, Self direction - Hedonism(zelf toegevoegd) - Benevolence. Ik kon me hier wel in voelen maar het is nog iets te generiek. Ik wil hier op een ander moment verder mee gaan naarmate van definitie etc. 
In ieder geval dat is opdracht 1 gedaan op naar opdracht 2.  
<br />
<br />
Opdracht 2. Hoe gedraag ik mezelf op social media. Voor dit moest ik eerst gaan kijken welke social media ik gebruik met gelijk de reden waarom/waarvoor ik ze gebruik. Deze zijn:
<br />
<ol>
    <li>Instagram(Soort blog. Connectie met vrienden)</li>
    <li>Whatsapp(Praten met familie en vrienden)</li>
    <li>Linkedin(Professioneel. Om bedrijven te zien.)</li>
    <li>Discord(Praten met vrienden)</li>
    <li>Pinterest(Inspiratie voor projecten etc.)</li>
    <li>Reddit(Plezier, Inspiratie voor projecten etc.)</li>
</ol>
<br />
Dus er is een patroon te zien. Ik doe veel met inspiratie op doen om ergens aan te werken en ik ben graag in contact met mijn vrienden. Dat komt in relatie terug met Innerlijke groei en vriendschap. 
Persoonlijk zijn dit de belangrijkste dingen voor mij in mijn leven. Ik leef mijn leven dus laten we er dan maar voor zorgen dat ik er plezier in heb en wat dingen heb geprobeerd en dat ik de dingen die ik probeer kan delen met me vrienden. 
<br />
<br />
Ik had op dit alles feedback gekregen van Micha en hij zei dat het wat meer in depth kan zijn en tot een extent kan ik wel zien dat dit best wel weinig is. Ik denk wel dat ik iets verder erop in kan gaan waarom ik dit belangrijk vind en waarom andere dingen niet zo belangrijk voor mij zijn. 
<br />
<br />
Daarom ben ik nog niet begonnen aan opdracht 3 maar heb er wel over nagedacht. Ik weet nog niet precies hoe ik het ga presenteren maar ik wil iets doen dat constante verandering laat zien. 
Misschien een foto collage of een animatie. Geen idee nog maar het moet gemaakt worden voor mijn manifest maar ook voor mijzelf. Zodat ik weet wie ik ben en waar ik sta want als ik er nu naar kijk sta ik ergens in een donkere kamer en heb ik een lege map die ik moet gaan vullen. 
En dat is het ongeveer wel voor deze dag. Hopelijk vond je het wat om te lezen en laat ik het dit keer eens anders afsluiten. Met een ethisch dilemma die bij me is gebleven van de les:
<br />
<br />
<b>Is mijn 10 jarige zoontje toegestaan om Fortnite te spelen?</b>