---
title: "Printen"
subtitle:  "in 3d"
date:  2018-05-25
categories: ["School","Kwartaal4"]
tags: ["Blog","School"]
---
Workshop 3d printen! <i>zonder te gaan 3d printen</i>
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Vandaag hebben een rondleiding gekregen rond het [stadslab](https://www.fablabs.io/labs/stadslabrotterdam). Beter laat dan ooit lijkt me dan maar. 
Bij het stadslab kan je veel dingen doen. Van 3d printen tot drones huren. Vrij interessant en nuttig voor de expo. Nuttig want je kan er ook A2 en A1 printen, posters dus!
Ik heb ook al met me groepje erover gehad om misschien iets te doen met posters of dergelijk zodat het er wat interessanter zal uit zien op de expo. 
