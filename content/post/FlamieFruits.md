---
title: "Listening in"
subtitle:  "Joined in on some calls"
date:  2019-11-20
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
Dus bij wat calls gezeten met docenten. Niks gezegd maar veel geleerd..
<!--more-->

<br />
<br />
<br />
<br />
Vandaag was een interessante dag omdat ik bij Ananda heb gezeten. Zij is Teacher Relations en zij focust erop om de docenten te stimuleren te blijven bij FeedbackFruits en om zich te richten op zichzelf ook te innoveren....<i>denk ik</i>. 
Bij deze calls hadden docenten wat vragen over hoe dingen werken en soms ook opmerkingen hoe iets momenteel werkt. Dit was super nuttig om bij te zijn omdat ik nu echt de werkwijze, binnen feedbackfruits, van docenten leerde kennen. 
Ook was het leuk om de enthousiasme en interesse van docenten te horen naar FBF toe. That's all tho...
<br />
<br />
<b><i>Peace Out. Girl Scout!</i></b>