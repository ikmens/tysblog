---
title: "Talking about talking"
subtitle:  "Podcast bespreken met Michelle"
date:  2019-11-08
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
Vandaag ben ik dan in Rotterdam omdat ik gister op werk was gebleven om het prototype te finaliseren. of tenminste zo dichtbij mogelijk te komen tot het finaliseren ervan...
<!--more-->

<br />
<br />
<br />
<br />
Maar vandaag niet heel veel bezig geweest met werk. Ik heb wat feedback doorgenomen van mijn begeleidster over mijn prototype voor toekomstige comments etc. en heb ik de hele dag met Michelle gehad over een podcast starten. 
We hebben een locatie gevonden om te filmen/recorden, besproken wie onze doelgroep is, waar we het over hebben, wat onze waardes zijn en op welke dagen we het gaan opnemen. Het enige wat we nog niet wisten waren een aantal technische dingen. 
We weten niet wat voor dingen Rolf had en of we dat uberhaupt willen gebruiken en als we het niet gebruiken hebben we zelf de spullen ervoor?, we wisten ook niet zo goed waar we onze podcast zouden moesten gaan hosten en als een eigen website nodig hadden. 
Want als dat het geval is moeten we ook een website ervoor gaan maken. Dus we zijn nog niet compleet klaar met alles in plannen maar we zijn wel weer van plan aankomende donderdag te meeten om het dan hierover te hebben en die dingen te gaan regelen zodat we zo snel mogelijk kunnnen gaan recorden en daadwerkelijk starten. 
<br />
<br />
<b><i>Peace Out. Girl Scout!</i></b>