---
title: "How might I"
subtitle:  "Let's get back into the groove"
date:  2019-03-27
categories: ["School","Year 2"]
tags: ["SemesterDos"]
---
Hoe kan ik weer terug in de flow komen van bloggen? oh, en de HMW ding dat ik heb gedaan met me team....
<!--more--> 

<br />
<br />
<br />
<br />
<br />
Dus er zijn redelijk wat dingen gebeurd in de tussentijd. Ik ben naar Maastricht gegaan met de eerstejaars, ben naar Berlijn gegaan met de derdejaars en ik mocht mijn keuzevak niet meer volgen vanwege deze redenen.
Maar het voelt alsof ik nog iets ben vergeten mhm......Oh ja, <b><i>Semester Dos!</i></b>. Ik zit nu in een nieuw groepje. Let me introduce you to team <b><i>Origami</i></b>:
<br />
<br />
<ul>
 <li>Sanne</li>
 <li>Ashana</li>
 <li>Hannad</li>
 <li>en natuurlijk ik!</li>
</ul>
<br />
<br />
Om een beetje info te geven over de opdracht. Onze opdrachtgever, NRC Handelsblad, vraagt aan ons een gepersonaliseerd nieuwsaanbod te ontwerpen waardoor er nog steeds genoeg <i><a href="https://nl.wikipedia.org/wiki/Serendipiteit">Serendipiteit</a></i> blijft voor de huidige lezers.
Super breed natuurlijk zorgt ervoor dat we lekker onze kant kunnen opgaan. Eerste wat we, voornamelijk mijn team sinds ik er niet was voor 2 weken, hadden gedaan was natuurlijk onderzoek. 
Deskresearch en Fieldresearch. Na dit allemaal hadden we een onderzoeksposter gemaakt en deze laten zien aan de opdrachtgever. Niet veel nuttige feedback gekregen omdat er niet veel tijd was. Hij was wel heel <i>NIEUWS</i>gierig wat de volgende stap zou zijn.
Was nog niet heel zeker dus hebben we wel netjes gezegd.
<br />
<br />
Vervolgens zijn we gaan kijken welke richting we op willen gaan. Ik had al iets ingedachten om te doen genaamd HMW(How Might We) dat ik in het boekje van <a href="https://ajsmart.com/">AJ&Smart</a> kreeg toen ik in berlijn was. Dus toen Ellen zij van misschien kunnen jullie HMW proberen schoot ik op de kans om dat te doen met me team. 
Post its, Markers, papier alles helemaal klaar gelegd en hebben we toen lekker gedaan. 
<br />
<br />
<img src="https://i.imgur.com/qADskMb.jpg" width="50%" height="50%"/>
<br />
<br />
Dit is dan een foto van post its waarin een paar HMWs staan. Helaas kan ik de andere niet vinden maar met de andere kwamen we tot het besluit om te gaan focussen op wanneer een lezer leest en niet op wat ze lezen. Dit zorgt ervoor dat we niet hoeven te focussen op het probleem van filter bubbles en hoeven we alleen te focussen op waar/wanneer we de lezer aanspreken.
Voor nu is dat alles dat ik me kan herinneren. Had ik maar iets waarin ik mijn dagen kon opschrijven huh... Maar ja misschien nog een song recommendation dan maar: 
<br />
<br />
<a href="https://www.youtube.com/watch?v=IrhgYrIc_T4" target="_blank"><img src="https://i1.sndcdn.com/artworks-BZouTBaMMcBq-0-t500x500.jpg" width="25%" height="25%" /></a>
<br />
<br />
Peace out Girl Scout!
