---
title: "Tekentoets"
subtitle:  ""
date:  2018-01-08
categories: ["Personal progression","School","Kwartaal2"]
tags: ["Blog","School"]
---
Tekentoets gehad vandaag op de 5de verdieping. Ik moest kubussen en cylinders in perspectief tekenen en een storyboard maken. 
Helaas ging storyboarding niet zo goed want ik wist geen verhaal te verzinnen. Hopelijk heb ik het gehaald en anders komt het waarschijnlijk door mijn storyboard.
