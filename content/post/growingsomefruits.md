---
title: "Talking about me"
subtitle:  "Growth talk reflectie"
date:  2019-11-06
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
Ik heb net me <u>Growth Talk</u> gehad en het was een fijne ervaring...
<!--more-->

<br />
<br />
<br />
<br />
Voordat ik in de details ga van de Growth talk laat ik eerst uitleggen wat het nou precies is.
<br />
<br />
Een growth talk is een <u>geen</u> evaluatie. Het is een gesprek waarin je kijkt naar je voortgang en wat je doelen zijn voor de volgende Growth talk over een kwartaal. Oke nu dat duidelijk is laten we het over deze Growth Talk hebben...
<br />
<h3>The talk</h3>
<br />
Aan het begin was ik best wel benieuwd naar wat er word gezegd in de Growth talk en hoe het nou echt zou gaan. Aan het begin gingen we eerst door de feedback die ik had gekregen en als ik me er in kon vinden. 
De feedback die gegeven over me vond ik mezelf ook in. 
<br />
<br />
<i>De feedback die mij werd gegeven in het kort ging over, <b>Plus sectie</b>,  dat ze het interessant vonden hoe snel ik mijn plaats kon vinden in het bedrijf, <b>Minus sectie</b>, dat ik nog moet leren hoe je werk gaat in een product bedrijf die zich op 1 ding focussen inplaats van heel veel kleine dingen creëren, <b>Interesting Sectie</b>, Dat ik geïnteresseerd ben in het gehele bedrijf en niet alle maar in "mijn sectie" van het bedrijf.</i> 
<br />
<br />
Na het hier over te hebben gehad ging het over wat meer persoonlijkere dingen. Hiermee bedoel ik dat we het gingen hebben hoe ik was als persoon omdat ze er geïntereseerd in waren. 
We hadden het over wat mijn interesses zijn dus wat vind ik leuk om te kijken, doen etc. Wat zijn mijn plannen voor mijn stage hier en hoe ik bij mijn studie ben gekomen. 
<br />
<br />
Zeker niet wat ik had verwacht. Ik weet niet eens wat ik had verwacht maar voor mijn gevoel niet dit. En dat is positief! 
<br />
<br />
<b><i>Peace Out. Girl Scout!</i></b>