---
title: "It's Friday!"
subtitle:  "Part 2"
date:  2018-04-13
categories: ["Personal progression","Kwartaal4"]
tags: ["Blog","School"]
---
Na de planning hebben we wat onderzoek gedaan over [Afrikaanderwijk](https://nl.wikipedia.org/wiki/Afrikaanderwijk) voordat we er naar toe gingen.
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Na het onderzoek zijn we rond 12 uur er naar toe gegaan. We zijn langs een paar dingen geweest. [De Botanische tuin](http://btarotterdam.nl/btarotterdam.nl/), het afrikaanderplein en Buurthuis de Arend.
Toen we bij de botanische tuin aankwamen werd ons gevraagd waarom we daar waren omdat er niet veel jongeren komen voor geen andere reden behalve voor school.
Ons is toen ook verteld dat jongeren de Botanische tuin niet "cool" vinden en er daarom niet zelf komen maar dat voornaamelijk ouderen komen en kinderen van de basisscholen.
Best wel jammer want hij zag er interessant uit en ik verwacht dat het in de zomer er nog beter uit zal zien.

<br />
<img src="https://i.imgur.com/P94Vczf.jpg" width="50%" height="50%"/>
<br />
<img src="https://i.imgur.com/VqmL5wf.jpg" width="50%" height="50%"/>
<br />
<img src="https://i.imgur.com/fOJM6FB.jpg" width="50%" height="50%"/>