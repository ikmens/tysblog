---
title: "Persona"
subtitle:  "Ze is naaktgeboren"
date:  2018-03-19
categories: ["School","Kwartaal3"]
tags: ["Blog","School"]
---
We zijn begonnen met een persona maken waarop we onze keuzes kunnen baseren.
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Onze persona is gebaseerd op wat onze doelgroep is.<br />
<ul>
<li>Doelgroep:</li>
Studenten HR<br />
18 tot 21 jaar oud<br />
Rotterdam Zuid & omstreken<br />
Thuiswonend<br />
<br />
Focus op voeding & beweging<br />
Focus op Nederlandse<br />
<br />
Geen focus op geslacht<br />
Geen focus op godsdienst<br /><br />
<li>Persona:</li>
Naam: 			Isabella Naaktgeboren, 18 jaar<br />
Hobbies:		Paardrijden (op pony)<br />
Persoonlijkheid:	Opwaarts mobiel<br />
Godsdienst:		Geen geloof<br />
Broers/zussen: 	broer Koen, 12 jaar<br />
Woonplaats: 		Brielle, Koningsdiep 24 (woont bij ouders)<br />
Vorige opleiding: 	HAVO<br />
Huidige opleiding:	HR ------<br />
Werkplaats:		Kassiaire Plus<br />
Vervoersmiddel:	Opoefiets, bus, metro<br />
<br />
</ul>
<img src="https://i.imgur.com/SmYkv88.jpg" alt="lifestylewords" width="500" height="600">