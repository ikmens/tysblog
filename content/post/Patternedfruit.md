---
title: "Once is a mistake"
subtitle:  "twice is a pattern"
date:  2019-12-24
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
Greetings...Ik ben dus alweer een aantal blogposts vergeten en ik vertel je wel waarom...
<!--more-->

<br />
<br />
<br />
<br />
Nou om de verwarring te clearen en mijn excuus voor het missen van steeds meer blog posts omdat ik het onderaan me lijst staat van prioriteiten. En nu ik ben begonnen met mijn stageverslag en alles daar omheen dus mijn blog is een beetje uit zicht gevallen. 
Ik vind het natuurlijk belangrijk om op me blog te posten maar andere dingen vind ik momenteel belangrijker. Om dan nog goed te zetten van de vorige paar dagen zal ik mijn Slack check-in's hier in doen. 

<h3>Dec 12</h3>
Hiya peeps :bongo: 
<br />
Today still in Roffa but will be working today! 
<br />
Focus is on:
<br />
- viewport of assignments :eye: 
<br />
- blog

<h3>Dec 13</h3>
Goodmorning everybody!
<br />
It's friday and ofcourse I had to forgot my notebook :)
<br />
So today I'll be focussing on:
<br />
- Research
<br />
- stageverslag
<br />
- blog

<h3>Dec 16</h3>
Hey everybody 
<br />
I don't feel very good and it started yesterday night. As a result of how I feel I've been kept awake all night and I'm fairly sleep deprived right now. I'll be staying at home today trying to catch up on sleep and recover. I'll be in the office tomorrow because this is an important week for me.
<h3>Dec 17</h3>
Greetings everybody 
<br />
Omw to FL. Most likely have to leave earlier today to take care of my lil' sister. But for the time I'm here I'll be focussing on:
<br />
- Design review/Criteria path 
<br />
- Viewport stuffs 
<br />
- Stageverslag stuffs

<h3>Dec 18</h3>
Heeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeey peoples
<br />
Sadly forgot my arduino so no lights for me :disappointed:
<br />
Today I'll be focussing on:
<br />
Stageverslag stuffs
<br />
Criteria stuffs
<br />
Viewport things

<h3>Dec 19</h3>
Hiya from the porcelain throne at the gym 
<br />
Will be in Roffa today :house: 
<br />
Today focus will be:
<br />
- Stageverslag (scanning in docs, doing some work, history of fbf)
<br />
- Criteria stuffs 
<br />
- Blog

<h3>Dec 20</h3>
Greetings peeps,
<br />
Feeling not so well so might not stay for the christmas party. On another note I brought my Arduino with lights but I'm suprisingly bad at putting them in the christmas tree so if someone could maybe improve placement would be great :merrychrystler:
<br />
Today I'll focus on:
<br />
Check-in with Esther
<br />
- Organigram
<br />
- Criteria stuffs

<h3>Dec 23</h3>
Hey hey hey everyone :bongo: 
<br />
Will be working from home mostly this and next week (planning to go to the office tomorrow and friday) so if anything is up send me a message. 
<br />
Today's plan is to focus on:
<br />
- sprintplanning 
<br />
- prep some stuffs 
<br />
- viewport 
<br />
- blog

<h3>Dec 24</h3>
Heeey let's make this a great tuesday!
<br />
Today I'll be focussing on:
<br />
- Viewport
<br />
- Design review
<br />
- Blog
<br />
- Photo's(?)