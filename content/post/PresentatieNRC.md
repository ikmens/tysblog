---
title: "Iets te veel moeite?"
subtitle:  "Origami Represent!"
date:  2019-04-05
categories: ["School","Year 2"]
tags: ["SemesterDos"]
---
We hadden vandaag onze concept presentatie voor Marije en een die ene man van NRC. Ging wel goed we waren lekker met de tijd ;-)...
<!--more-->

<br />
<br />
<br />
<br />
Dus we hadden de concept presentatie vandaag. Ging super goed ze waren zeker geïnteresseerd in onze concepten. Dus laat ik uitleggen welke concepten we hebben nu:
<br />
<br />
<ul>
    <li><i>Jouw Nieuws</i></li>
    Jouw nieuws is een personalisatie methode die de gebruiker ultieme transparantie bied. Ze kunnen zien welke artikelen ze gelezen hebben, ze kunnen artikelen uit hun lees geschiedenis halen zodat ze niet mee tellen in het algoritme en de categorieën worden geordend op wat ze het meest lezen. 
    Ik was verantwoordlijk voor het prototype voor dit concept dus, me being me, heb ik iets te veel moeite erin gestoken en had ik een animatie ervoor gemaakt. Als je hem wil zien kan je hier <a href="https://youtu.be/BT-UQqr3PRk">klikken</a>
    <br />
    <br />
    <li><i>Find the Time</i></li>
    Find the time is een personalisatie die kijkt naar hoe lang de gebruiker artikelen leest. Het is geen traditionele manier van personalisatie gebaseerd op leesgeschiedenis. 
    Met dit concept willen ervoor zorgen dat alle gebruikers een artikel kunnen lezen ook al hebben ze niet veel tijd. Als het algoritme op merkt dat de gebruiker snel afhaakt bij lange artikelen of op bepaalde moment voor een korte tijd de app/website gebruikt kan het een variatie van het artikel bieden. 
    Deze variatie is een verkorte/samengevatte versie ervan. Dit kan een iets korter artikel zijn of mogelijk een soort video die het onderwerp introduceert/uitlegt. 
    <br />
    <br />
    <li><i>NRC Story</i></li>
    NRC Story is een nieuwe manier van nieuws consumptie samen met een "oud" medium. Met NRC Story zou je een soort nieuws moment naar je Tv kunnen laten zenden waarin je zelf (of het algoritme) hebt gekozen welke artikelen je wilt zien. 
    Omdat sommige gebruikers nog steeds op de Tv het nieuws kijken kunnen ze zo nog steeds het NRC volgen maar dan via hun gewenste medium.
    <br />
</ul>
<br />
Anyhow, ze waren helemaal verkocht. Ze vonden ze alle 3 hartstikke interessant met een preferentie naar <i>Find the time</i> omdat deze met tijd te maken had en andere teams hadden niet zoiets soort gelijks. Ze vonden het ook fijn dat deze keer alle teams met iets anders kwamen inplaats van dat iedereen ongeveer hetzelfde herhaald. 
Dus ja dat was nice.
<br />
<br />
<b><i>Peace out. Girl Scout!</i></b>
