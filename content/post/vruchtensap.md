---
title: "Terugkomdag numero tres"
subtitle: "Prepping for report"
date:  2019-10-29
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
Terug naar school. Met mensjes gesproken en voorbereid op stageverslag schrijven...
<!--more-->

<br />
<br />
<br />
Vandaag was een terugkomdag voor school dus ik was in Rotterdam vandaag wat wel fijn was. Niet heel veel gedaan aan werk vandaag wat wel minder is. Bij de terugkomdag hebben we het gehad over het schrijven van het stageverslag en hoe we te gang kunnen gaan. 
Als hulp hebben we een oefening gedaan om te kijken hoe ver we waren met een leerdoel. We gingen elkaar interviewen. 1 op 1. Ik ging met Savannah. <i>gelukkig, super chill met haar</i>. 
Tijdens het interview moesten we dan vragen aan elkaar stellen naar richting van STARRT dus <b>S</b>ituatie, <b>T</b>aak, <b>A</b>ctie, <b>R</b>esultaat, <b>R</b>eflectie, <b>T</b>ransfer. 
Was interessant en heel nuttig. Savannah heeft me ook het blaadje gegeven waarop ze heeft geschreven wat ik had gezegd wat supppppppppppeeer nice en handig is voor wanneer ik echt begin met schrijven. 
En dat was wel mijn dag...
<br />
<br />
<b><i>Peace Out. Girl Scout!</i></b>