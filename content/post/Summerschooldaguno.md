---
title: "Summerschool"
subtitle:  "Dag 1"
date:  2018-06-18
categories: ["School","Kwartaal5"]
tags: ["Summerschool"]
---
Helaas niet al mijn competenties behaald tijdens mijn assessmentgesprek. Wel de mogelijkheid gekregen om naar Summerschool te gaan.
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Dag 1 van Summerschool zit erop. We hebben wat informatie gehad over wat er gaat gebeuren en wat we moeten gaan doen. Voor vandaag moesten we een longlist/shortlist maken waarin we besluiten welke <b>Locatie/Doelgroep/Bedrijf</b> we kiezen.
Ik had al redelijk snel een longlist afgemaakt en deze naar een shortlist gemaakt. Ik bleef alleen nog vast zitten op 4 opties voor de 3 categorieën.
Door wat te googlen vond ik een bedrijf dat ik wil gebruiken. De [Fietsersbond](https://www.fietsersbond.nl/?gclid=EAIaIQobChMI-MCbluTd2wIVRBobCh0IKAoFEAAYASAAEgIfL_D_BwE)(hopelijk mag het).
De fietsersbond is vooral gefocust wat het best is voor fietsers en heeft zelf niet echt veel behalve een website en magazine. Mijn <b>Locatie/Doelgroep/Bedrijf</b> zijn dan geworden: 
<br />
<br />
<ul>
<li> Locatie: Den Haag </li>
<li> Doelgroep: Internationale Studenten </li>
<li> Bedrijf: De Fietsersbond </li>
</ul>
<br />
<br />
Hopelijk zijn er geen problemen mee anders verander ik het en kan je dit lezen in mijn volgende blogpost.