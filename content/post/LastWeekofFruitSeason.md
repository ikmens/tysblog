---
title: "Last bits"
subtitle:  "Last week of the season"
date:  2019-11-18
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
Laatste week! Vandaag ben ik aan het werk gegaan met mijn projecten...
<!--more-->

<br />
<br />
<br />
<br />
Dus ja aan mijn projecten gewerkt. Niet heel veel bijzonders. Bezig geweest met het het digitaliseren van schetsen en mijn andere project aan het schoonmaken. That's all.
<br />
<br />
<b><i>Peace Out. Girl Scout!</i></b>