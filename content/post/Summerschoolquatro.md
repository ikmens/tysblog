---
title: "Summerschool"
subtitle:  "Dag 4"
date:  2018-06-21
categories: ["School","Kwartaal5"]
tags: ["Summerschool"]
---
Donderdag! Feedback gekregen over mijn ontwerpdoel, onderzoeksplan en onderzoeksrapportage. Wat was ik aan het doen?
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Dag 4 bijna alweer op de helft van Summerschool. Feedback van Mieke gekregen dat me wel even een wake-up call gaf en daar ben ik heel blij om. 
Dus de feedback die ik heb gekregen van Mieke was dat mijn ontwerpdoel nog niet duidelijk genoeg was en daarom mijn onderzoek ten koste is gegaan. 
Daardoor heb ik wel informatie kunnen verzamelen maar niet iets waarmee ik kan gaan werken en daar ben ik het wel mee eens toen ik terug keek naar mijn ontwerpdoel. 
Dus dat heb ik gelijk aangepast en aangescherpt zodat ik dit keer wel field research kan doen waar ik daadwerkelijk iets aan heb. 
<br />
<br />
Stap 2 was dus om field research te doen. Hoe? Ga ik nog een keer interviews plegen in Den Haag? Ga ik nu gewoon mensen interviewen op straat? Nee dat is niet echt nodig.
Ik kan een enquête rond sturen. Maar eerst dan maar maken. Waar ga ik over vragen? Ik denk dat ik ga vragen over tours sinds het bedrijf waar ik voor heb gekozen [foodtours](http://tastetrekkers.com/what-is-a-food-tour/) doen.
Ik graag zien hoeveel mensen een tour hebben gevolgd, wat ze ervan vinden en voor de mensen die nog niet op een tour zijn gegaan of ze ooit op een tour zouden gaan en waarom ze nog niet op een tour zijn geweest.
<br />
<br />
Dus mijn onderzoeksplan opgesteld en feedback gevraagd aan Fedde. Gelukkig waren deze keer mijn ontwerpdoel en onderzoeksplan wel duidelijk genoeg en hadden ze een nuttig doel. 
Na deze feedback dus mijn gehele enquête opgesteld, een paar mensen het laten testen om te zien als de volgorde klopte en daarna opgestuurd naar allerlei mensen. In de Summerschool groepsapp, naar mijn moeder en wat mensen die ik ken.
Hopelijk heb ik dan morgen minstens 20 resultaten zodat ik de informatie nuttig vind om te gebruiken. 
<br />
<br />
Dat was alles dat ik vandaag heb gedaan. Tot morgen <b><i>Peace out!</i></b>