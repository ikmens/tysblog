---
title: "Geen stackedit meer!"
subtitle:  "Nu atom gebruiken"
date:  2017-09-18
categories: ["Personal progression","Kwartaal1"]
tags: ["Blog"]
---
Geen stackedit meer voor de blog maar inplaats daar van [Atom](https://atom.io/).
Dit zorgt ervoor dat ik gemakkelijker en offline blog posts kan maken.
