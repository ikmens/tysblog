---
title: "Helemaal afgekraakt ;-("
subtitle: "Jk... Reflectie op stagebezoek"
date:  2019-10-30
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
Pia is net langs geweest voor het stagebezoek en hier is mijn reflectie erop nu ik het nog vers in herrinering is...
<!--more-->

<br />
<br />
<br />
Pia is langsgekomen voor het stagebezoek. In dit stagebezoek hebben we het erover gehad hoe ik ben geweest in de laaste 5 weken van mijn stage samen met mijn bedrijfsbegeleidster(Esther).
<br />
<br />
Het gehele gesprek was, in mijn blik, hartstikke positief en heel erg goed gericht op de punten waar ik me op kon verbeteren want hoewel het leuk is om te horen dat je het goed doet is het vele malen nuttiger om te weten hoe je het nog beter kan doen.
<br />
<br />
Ik merkte wel dat tijdens dit gesprek niks "nieuws" boven kwam. Wat ik hiermee bedoel is dat alle dingen die waren gezegd ik al eerder heb gehoord of zelf ook al had opgemerkt. 
Wat niet erg is want dat valideert mijn perspectief op mezelf wat ook fijn is. De reden dat niks bijzonders naar boven kwam is omdat elke 2 weken ik en Esther al check-ins hebben en soortgelijke dingen. 
Dus als dingen fout gaan of beter kunnen komen ze hier aanbot. Maar ook al is er niets nieuws opgekomen staat het nu een soort van officieel. 
<br />
<br />
Om een opsomming te geven van wat er dan is gezegd in het gesprek is dat ik <b><i>om veel sturing vroeg</i></b> en ik zie mezelf dit ook vaak doen. En dat is zeker een groeipunt van me. 
Dit "om sturing vragen" doe ik heel vaak omdat ik bang ben dat ik anders te ver van het pad afdwaal en werk ga doen dat nutteloos gaat zijn als er op terug word gekeken. Maar het kan wel wat minder en dat is iets waar ik nu, zeker door deze feedback, wat strenger op ga letten. 
Want soms hindert het me ook met verder werken. Dan wacht ik totdat ik antwoord heb op iets ook al kon ik ook verder werken en zou het goed zijn gegaan. 
<br />
<br />
Buiten dit heb ik niet veel meer om toe te voegen so...
<br />
<br />
<b><i>Peace Out. Girl Scout!</i></b>