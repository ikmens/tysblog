---
title: "It's friday!"
subtitle:  "Part 1"
date:  2018-04-13
categories: ["School","Kwartaal4"]
tags: ["Blog","School"]
---
Het nieuwe kwartaal is een soort van begonnen. We hebben een wijk gekozen om op te focussen([Afrikaanderwijk](https://nl.wikipedia.org/wiki/Afrikaanderwijk)) en we zijn op school om een planning te maken.
Het eerste wat we hebben gedaan is gaan kijken welke competenties iedereen nog moet behalen. Met deze informatie hebben we beroepsproducten gekozen die we willen maken om te kunnen gebruiken in onze STARRTs.
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
<img src="https://i.imgur.com/gdAtRhh.jpg" width="50%" height="50%"/>
<img src="https://i.imgur.com/10f0uOh.jpg" width="50%" height="50%"/>
<br />
<br />
Daarna hebben ik en Meintje gekeken waar en wanneer alles gemaakt kan en moet worden. Dit hadden we toen in de planning geschreven samen met wie het product zal maken.
<br />
<img src="https://i.imgur.com/Rfyv6cm.jpg" width="50%" height="50%"/>
<img src="https://i.imgur.com/raCNrBe.jpg" width="50%" height="50%"/>