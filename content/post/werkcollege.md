---
title: "Trashcan prototype"
subtitle:  "trash trash baby"
date:  2017-09-23
categories: ["Personal progression","Kwartaal1","School"]
tags: ["Blog","Werkcollege"]
---
Bij de werkcollege gingen we werken met het [Double diamond](https://d2u0o0plmpbj03.cloudfront.net/uploads/attachment/file/1094/article_bothdd.png?v=1464246734) model. Wij hadden een opdracht gekregen om in die les iets te verzinnen dat de eerstejaarsstudenten met elkaar zou verbinden doormiddel van/met een prullenbak.
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Dus waren we gaan brainstormen over wat voor de hand liggende dingen waren om de prullenbak een verbindend element te geven.
>Dit waren:
>
- Basketbalring boven prullenbak
- Audio feedback (efteling)
- Visuele feedback

&nbsp;

> Toen gingen we over naar de niet zo voor de handliggende ideeeën:
>
- Speaker in prullenbak
- Robot achtig
- Personalized
- Bokszak

&nbsp;
>En dan als laatste ideeën die tegenwerken:
>
- Weglopend
- Niet fijn hoog geluid als iemand dichtbij komt
- Niet werkende prullenbak

Toen moesten we er 2 kiezen om te gaan prototypen. Deze 2 werden _Personalized_ en _Bokszak_.
![](https://68.media.tumblr.com/b8e99085b535d5634125c5313e4ca578/tumblr_owpziywRbU1sj8og3o2_1280.jpg)

Hiervan hebben we een kleinere versie van de personalized prullenbak gemaakt en deze hebben we bij een groepje neergelegd om voor hen om te versieren, wat zij toen hadden gedaan. En we merkte op dat toen we hem gingen ophalen dat ze het jammer vonden om terug te geven dus daardoor hadden we door dat het een redelijk leuk idee was om mee te gaan.

Deze werkcollege heeft mij geleerd dat hoewel je idee vrij absurd lijkt het nog best wel iets goed kan zijn als je maar genoeg moeite erin steekt.
