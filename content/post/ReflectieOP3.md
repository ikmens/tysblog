---
title: "Reflectie kwartaal 3"
subtitle:  "Mirror on the wall!"
date:  2018-04-11
categories: ["School","Kwartaal3"]
tags: ["Blog","School"]
---
Het kwartaal is voorbij, peerfeedback is gegeven, de STARRTS geschreven en een competentie is gekregen.
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Het kwartaal zit er weer op. Tijdens dit kwartaal was ik Teamcaptain van het Team "De Fetchtables". Tijdens dit kwartaal hebben we onderzoek gedaan op verschillende scholen, concepten bedacht en een klein beetje gewerkt aan een prototype
In deze blogpost ga ik daarop op terug kijken en vertellen waar ik vind dat ik volgend kwartaal (OP4) aan moet werken en wat ik goed vond gaan de volgende keer.

<br />
<br />
Om te beginnen zal ik terug kijken op het [plan van aanpak](https://docs.google.com/document/d/1taeY_8iM5apa8GUbf2OUyN9xKEBTG3Cll9bcgAs4kDc/edit?usp=sharing) waar ik aan heb gewerkt:
<br />
Het was de eerste keer dat ik heb gewerkt aan een plan van aanpak. Dus er waren wat fouten in het begin die ik over het hoofd had gezien aan het begin en later in het kwartaal verbeterde nadat ik feedback gehad eroveer.
Na deze verbeteringen had ik het nog een keer ingeleverd om meer feedback te krijgen wat ik toen ook had gekregen. [Feedback](https://imgur.com/a/Oc89c). Deze feedback gaat mij super veel helpen in het maken van een plan van aanpak voor kwartaal 4. 
Het liet zien dat sommige dingen die we krijgen van de opdrachtgever eerst moet geverifieerd worden voordat we het aannemen als waar. Ook liet het me zien dat ik nog schreef alsof het alleen werd gelezen door mensen die al weten waar ik het over had.
<br />
<br />
<!--more-->
Als volgt ga ik kijken naar de [lifestyle diary(toekomstige situatie)](https://drive.google.com/a/hr.nl/file/d/1A1WFhsJioll_gyWN5gPGT2OzBEjdWfE6/view?usp=sharing)(visualisatie gemaakt door Meintje):
<br />
Deze lifestyle diary is gemaakt door een combinatie te maken van onze(het team) ideale/gezondst mogelijke twee dagen. Om de twee dagen te maken had ik het team geinstructeerd om hun ideale twee dagen op te schrijven op [papier](https://imgur.com/a/IFjjV). 
Hiermee had ik toen een twee dagen LD(Lifestyle Diary) op geschreven die toen is gevisualiseerd door Meintje. Deze lifestyle diary vond ik persoonlijk niet super handig. Dit kan zijn omdat ik er niet genoeg informatie had in gezet of omdat ik nog niet helemaal wist wat ik er allemaal voor informatie kon uit halen.
Deze keer was de LD dus gemaakt door Meintje maar ik denk dat ik hem volgende keer, als ik ooit nog een LD ga maken, zelf maak om zo misschien de wat meer verstopte informatie te voorschijn haal.
<br />
<br />
Nu komt de brainstorm sessie aan bot. Deze sessie had ik al besproken in een vorige [blogpost](https://ikmens.gitlab.io/tysblog/post/technic/) maar nu weet ik wat ik wil verbeteren voor volgende keer:
<br />
<i>Voor de uitleg van de sessie lees de oudere blogpost.</i> Wat ik hieraan wil verbeteren is dat ik deze keer alleen het convergeren had ingepland maar niet het divergeren. Hoewel ik wel met het team het divergeren heb gedaan was dat op een ander moment en was dit niet met een plan. De volgende keer zal ik dan ook van start tot eind het inplannen.
Van het divergeren tot het convergeren zodat er wat meer structeer in zal zitten en omdat ik dan wat dingen kan klaar zetten indien nodig.
<br />
<br />
Last but not least het peerfeedback moment. Deze had ik ook al besproken in een eerdere [blogpost](https://ikmens.gitlab.io/tysblog/post/demfeedbackdongles/). Dus voor de resulataten van het moment kijk naar de vorige post:
<br />
Ik vond het vrij goed gaan. Er was goede feedback gegeven die ik kon gebruiken om mezelf te verbeteren in het volgende kwartaal. Maar het was wel weinig omdat het maar 1 keer was en deze keer was aan het eind van het kwartaal.
Dit wil ik ook gaan veranderen door wekelijks een peerfeedbackmoment in te plannen waarin dan word terug gekeken naar de afgelopen week om te zien wat ze vinden dat er beter kon gaan die week en wat anders kan die week. Dit zal voor meer feedback zorgen en zal ook ervoor zorgen dat we kunnen oefenen met feedback geven.
<br />
<br />
Dit was mijn reflectie voor kwartaal 3 bedankt voor het lezen. 