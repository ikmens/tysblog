---
title: "Wrapping up and looking further"
subtitle:  "Prototype building"
date:  2019-10-22
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
Let's wrap this up. Ik wil gaan testen dus ik moet het morgen zoiezo af hebben!...
<!--more-->

<br />
<br />
Vandaag echt voornamelijk gewerkt aan het prototype en het is nog steeds niet af! Echt iets van 6 uur mee bezig geweest maar telkens alle spacings en whatever doen zuigt zoveel tijd op dat ik nu pas op scherm 8 ofzo ben van de x hoeveelheid. 
Buiten het prototype had ik ook me Design Alignment meeting. In deze meeting bespreken we hoe ver we zijn met dingen, of we nog dingen moeten weten en/of er dingen fout zijn gegaan zodat we op 1 lijn zitten. 
Ook bedacht ik me vandaag dat ik voornamelijk moet gaan testen bij studenten. Op hogescholen. Maar het is herfstvakantie. Dus ja daar moet ik nog even op wachten. Gelukkig hebben ze me wel een nieuw project alvast gegeven om naar te gaan kijken wanneer ik klaar ben met het prototype zodat ik niet niks aan het doen ben tot volgende week. 
Ik ga kijken naar concurrenten met een bepaalde blik. Kan helaas alleen niet vertellen waarop ik ga letten ;^). Anyhow that's all for today.
<br />
<br />
<b><i>Peace Out. Girl Scout!</i></b>