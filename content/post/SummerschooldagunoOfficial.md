---
title: "Summerschool"
subtitle:  "Dag 2"
date:  2018-06-19
categories: ["School","Kwartaal5"]
tags: ["Summerschool"]
---
Dag 2 of de eerste volledige summerschool dag. Veel gedaan zoals field en desk research. Plannen gemaakt voor morgen maar jammer genoeg nog geen feedback gevraagd.
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Dag 2! Kwam op school aan en we kregen te horen dat we in groepjes onderzoek gingen doen naar concurrenten en daarna ons onderzoek delen met een ander groepje. 
Ging wel soepel. Ik zat in een groepje met Savannah, Jari en Meintje. Wij gingen [Greenwheels](https://www.greenwheels.com/global) onderzoeken. Nog nooit van gehoord voor dit project.
Blijkbaar is het dus een auto verhuur bedrijf rondom Nederland en Duitsland. In deze landen zitten de auto's verspreid die je dan kan huren via een app. Voor een prijsje natuurlijk.
Uit ons onderzoek bleek dus ook dat het het best is als je het alleen gebruikt voor sporadische momenten als je snel iets nodig hebt in de stad want dan kost het je niet te veel.
Ga je vaak een weekendje weg dan is het misschien beter om een auto te kopen. 
<br />
<br />
Het onderzoek van het andere groepje (Shreyas, Isaiah, Prewish en nog iemand <i>sorry je naam vergeten</i>) ging over OV. Zij hebben dan gekeken naar metro, bus en OVFiets vooral.
Zij zijn erachter gekomen dat mensen het OV syteem makkelijk en fijn te gebruiken vonden alleen de prijzen werden wel hoger wat wel een minpunt was. Ook moet je betalen voor de OVFiets ook al heb je een studentenOV hebt wat wel jammer is.
De OV-app waarop je kan zien wat je saldo is en je reisgeschiedenis was niet bekend hoewel mensen wel zeiden dat ze het fijn zouden vinden als er zoiets zou zijn. 
<br />
<br />
Daarna gewerkt aan mijn ontwerpdoel. Was redelijk makkelijk op te stellen sinds ik al me een visie voor me had en er al een tijdje over na zat te denken tijdens het onderzoek.
Helaas kon ik nog geen feedback krijgen erover sinds ze gingen afronden. Dat zal ik dan ook morgenochtend doen sinds dan Bob op school is.
<br />
<br />
Morgen is het plan ook om vroeg op school te zijn om een onderzoeksplan op te stellen en deze dan uit te voeren in de middag. <i>Hopelijk!</i>
<br />
<br />
Ook heb ik nog een vlog gedaan over deze dag hopelijk vind je het wat! 
<br />
<iframe width="560" height="315" src="https://www.youtube.com/embed/2JNltF1hFQg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<br />
<br />
Dat is het voor de blog van vandaag tot morgen! <b><i>Peace out!</i></b>