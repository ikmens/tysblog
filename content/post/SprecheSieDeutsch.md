---
title: "Sorry wat zeg je"
subtitle:  "Ik spreek geen Nederlands"
date:  2018-09-25
categories: ["School","Year 2"]
tags: ["SemesterUno"]
---
Yay weer school! We gaan weer verder met de onderzoeksposter. Nu hebben we de onderzoek die we willen hebben gaan we kijken wat we nog moeten doen om een poster te maken.
<!--more-->

<br />
<br />
<br />
<br />
<br />
Dus het onderzoek is binnen van fieldresearch. We kwamen erachter dat bij onze locatie er vooral mensen zijn met een achterstand of totaal geen kennis in Nederlands. 
Hierop willen we dus ook gaan focussen. Om precies te zijn: <i><b>Mensen in de sociale huur die geen tot weinig Nederlands spreken.</b></i> 
<br />
<br />
We moeten voor het einde van de sprint een "Onderzoeksposter" opleveren. In deze poster kan van alles staan. Om ons een idee te geven hebben ze op de beamer een soort template gegeven.
<br />
<br /> 
<img src= "https://i.imgur.com/5u6OlH3.jpg" width="50%" height="50%"/> 
<br />
<br />
Als team willen we de dingen op deze template gaan maken. Naja we moeten het wat duidelijker opschrijven omdat we het eigenlijk al gedaan hebben. Daarna gaan we kijken hoe we de poster eruit gaan laten zien. 
Dus dat is hoever we zijn nu. :)