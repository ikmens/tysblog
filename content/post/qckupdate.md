---
title: "Still alive"
subtitle:  "It's been a while"
date:  2019-05-06
categories: ["School","Year 2"]
tags: ["SemesterDos"]
---
Snelle recap van de dingen die ik in de "vakantie" heb gedaan...
<!--more-->

<br />
<br />
<br />
<br />
<b>Invision</b>
<br />
<br />
Ik heb gewerkt met invision voor de eerste keer. Super gaaf en super tevreden ermee. Als eerste prototype ermee had ik "Jouw Nieuws" gemaakt erin. Het concept gaan we waarschijnlijk niet verder gebruiken maar het was meer voor het uitproberen. 
Het duurde me wel even om erachter te komen hoe alles werkte en om de schermen te maken in Invision sinds ze niet vanaf illustrator kunnen komen vanwege redenen. Na al de moeite van de schermen weer na maken hoefde ik alleen nog de animaties er in te doen en nu lijkt het alsof het een echte feature van de app is!!!! 
Hier nog een gif van hoe het eruit ziet. Enjoy!
<br />
<video width="50%" height="50%" controls>
<source src="https://i.imgur.com/ILzpT5K.mp4" type="video/mp4"/>
</video>
<br />
<br />
<b>Arduino</b>
Ook heb ik deze vakantie een Arduino gekocht. Super leuk om te gebruiken. Ook super gaaf het ding zelf. Het geeft een soort gevoel van finalizing omdat het er zo technies uit ziet en dat is iets dat ik miste bij CMD en gelukkig nog wel bij CT lab kreeg. 
Met mijn Arduino (en wat led strips) wil ik mijn tas gaan "upgraden". De code voor de Arduino hebben kunnen salvagen van de Demoreel die in een library zat van <a href="http://fastled.io/" target="_blank">FastLED</a> zat. 
Op school wil ik nog wat gaan solderen op een print plaatje zodat de kabels er niet telkens uitvallen en daarna wil ik de leds aan mijn tas gaan naaien (denk ik) en dan Voilá. For now though. <b><i>WIP</i></b>
<br />
<br />
<b><i>Peace out! Girl Scout</i></b>