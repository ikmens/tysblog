---
title: "The calm before the storm Part 2"
subtitle:  "It was sorta useful"
date:  2019-04-01
categories: ["School","Year 2"]
tags: ["SemesterDos"]
---
Great minds think alike zelfs over brainstorm technieken....
<!--more--> 

<br />
<br />
<br />
<br />
Dus het brainstormen is gedaan! Het was super leuk en interessant. We begonnen met een <b>Negative Brainstorm</b>. Deze ging wat lastig aan het begin omdat we eerst onze ontwerpvraag moesten herformuleren waar we nog mee bezig waren voor het verslag. 
Na iets van een kwartier was dat gelukt zijn we begonnen. Het was niet moeilijk om hier mee te beginnen omdat iedereen in het team er al bekend mee was. Vervolgens zijn we gaan positive brainstorming a.k.a. brainstorming met de negative brainstorming in gedachte. 
<br />
<br />
<img src="https://i.imgur.com/iQvb1A8.jpg" height="50%" width="50%"/>
<br />
<br />
Vervolgens zijn we met de Lotus methode verder gegaan. De methode waar ik het <a href="https://ikmens.gitlab.io/tysblog/post/prebrainstorm/" target="_blank">eerder vandaag</a> over had! Sanne wou deze ook doen!
Deze methode was wel lastig omdat dit de eerste keer is voor iedereen. Beetje uitleg en een energiser verder en we waren er mee klaar.
<br />
<br />
<img src="https://i.imgur.com/R1T6UB5.jpg" height="50%" width="50%"/>
<br />
<br />
We hadden wat dingen samen gevoegd om zo 2 concepten te verzinnen waarmee we iets kunnen morgen(dinsdag). Maar voor nu heb ik niks meer toe te voegen behalve dat ik het hoorcollege voor 45% interessant vond en dat ik CMGT 1e jaars heb geholpen.
<br />
<br />
<b><i>Peace out. Girl Scout!</i></b>