---
title: "Working TOO hard"
subtitle:  "Recap laatste 3 dagen"
date:  2019-10-18
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
Dus ja, blijkbaar kan je zo hard werken dat het moeilijker word voor je om te werken en dat nog wel in me eerste maand...
<!--more-->

<br />
<br />
<h2>Uitleg</h2>
<br />
Over deze laatste 3 dagen had ik heel veel last van concentratie problemen. Ik denk dat dit komt omdat ik in de laatste 4 weken. Elke dag maar iets van 2 uur voor mezelf heb gehad en hierdoor een soort burned out ben geworden. 
Op woensdag middag(16-10-2019) begonnen de symptonen al. Ik kreeg moeite met focussen op me taken van die dag. Op donderdag was het top punt van moeite. Eerst zat ik 4 uur in de trein vast daarna kwam ik op werk en kon ik nergens op focussen. 
Dus meteen besproken met HR. Op vrijdag werkte ik van thuis en ging ik naar school met nog steeds moeite in concentreren. 
<br />
<br />
<h3>Woensdag</h3>
<br />
Focus vandaag was op spacing in me design, iets wat meer voor de developers later is dan voor testen nu. Rond 2-3 uur merkte ik dat ik moeite kreeg met focussen op mijn taken en besloot wat dingen te lezen tot het einde van mijn werkdag.
<br />
<br />
<h3>Donderdag</h3>
<br />
Zat vast in de trein voor 4 uur. Vervolgens had ik me design alignment met Esther. Ik merkte een soort waas over me. Na de lunch had ik een meeting met HR omdat ik nog wat vragen had over administratieve dingen. 
Tijdens de meeting hadden we het over wat mailtjes, vakantiedagen, werken van huis etc. etc. Ook vertelde ik over mijn moeite met concentreren. De waas die ik eerder al merkte. 
Ik kon me nergens op focussen en het ergerde me omdat ik bijna klaar was met dit project. Ze gaf met wat tips over wat ik kon doen om dit te verhelpen/voorkomen en zei dat ik met Esther het hier over moest hebben omdat zij hetzelfde probleem had toen het kantoor nog ergens anders stond. 
Dus na de meeting weer een gesprek met Esther gehad. <i>We waren een rondje gaan lopen.</i> Zij gaf me wat tips die ze zelf gebruikte toen hetzelfde bij haar gebeurde. Tijdens dit gesprek hadden we afgesproken om ook te kijken naar hoe het zou gaan als ik 1x per week van huis werkte. 
Zodat ik niet zo lang hoefde te reizen elke dag en dan wat meer tijd voor mezelf zou hebbben na werk. Na dit allemaal had ik besloten om weer naar huis te gaan en te gaan rusten omdat het vandaag echt niet zou lukken om iets nuttigs te gaan doen. 
<br />
<br />
<h3>Vrijdag</h3>
<br />
Voelde me al een stuk beter vergeleken gister maar nog steeds moeite met concentreren. In de ochtend had ik besloten wat rustiger aan te doen en heb ik gekeken naar hoe de flow van mijn prototype zou zijn en gekeken naar hun component guide. 
In de middag ging ik naar school toe om met Michelle te ontmoeten. Ze ging me laten zien hoe haar Design System in elkaar zat en gaf me wat tips voor als ik er zelf eentje wou opzetten omdat ik daar nog over zit te twijfelen. 
Na hier naar gekeken te hebben gingen we naar de Prototype Party van de 2e jaars. Een beetje feedback gegeven aan hen en met Rolf naar Myrthe gegaan om haar over te halen om een invite link door te sturen van de Stagiair Slack-workspace. Na dit allemaal ben ik naar huis gegaan. 
<br />
<br />
<b><i>Peace Out. Girl Scout!</i></b>