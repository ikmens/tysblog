---
title: "Expo"
subtitle:  "Showing everything"
date:  2018-06-01
categories: ["School","Kwartaal4"]
tags: ["Blog","School"]
---
Het is expo dag! De dag waar we naar toe hebben gewerkt dit laatste kwartaal. De app is tentoongesteld en allerlei mensen vonden het super gaaf!
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Tijdens deze expo hebben onze app laten zien samen met 2 posters waarop wij informatie lieten zien over ons onderzoek en project.
<br />
<img src="https://i.imgur.com/FZmTlfo.png" width="50%" height="50%"/>
<br />
<img src="https://i.imgur.com/mk39xCu.png" width="50%" height="50%"/>
<br />
<br />
De eerste poster is gemaakt door Meintje en de andere is gemaakt door mij met schermen die ik en de rest van het groepje hebben gemaakt. 
Deze posters zijn geprint in het stadslab waar ik in een [vorige post](https://ikmens.gitlab.io/tysblog/post/workshopprint/) over heb geschreven.
De versiering voor onze stand is gemaakt/gekocht door Meintje, Sophie en Savannah.
<br />
<img src="https://i.imgur.com/H6Su3ng.jpg" width="50%" height="50%"/>
<br />
Bij deze stand heb ik aan verschillende mensen uitgelegd wat ons concept is en heb ik meerdere keren van verschillende personen gehoord dat het een interessant/leuk idee is en dat ze het zelf ook zouden downloaden.
<br />
<img src="https://i.imgur.com/kRostOw.jpg" width="50%" height="50%"/>
<br />
<br />
Aan het einde van de expo kwam Albert en vroeg wat goeie vragen zoals "<i>Wat maakt deze app anders dan die andere online kookboeken?</i>" en "<i>Wat zorgt ervoor dat mensen de app downloaden?</i>".
Het was een fijne verandering van de mensen die niet veel vroegen over de app en vooral positieve feedback gaven.
<br />
<br />
In het algemeen vond ik de expo een succes en ben ik blij met hoe alles is uitgepakt.