---
title: "Summerschool"
subtitle:  "Dag de rest"
date:  2018-06-29
categories: ["School","Kwartaal5"]
tags: ["Summerschool"]
---
Ik had het heel druk deze laatste week dus hier heb je een samenvatting van deze week.
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Na de interviews gedaan te hebben moest ik ze transcriberen en daarna de belangrijkste uitkomsten daarvan in mijn onderzoeks rapportage doen. [Leef je uit](https://docs.google.com/document/d/187pZGcYC-6YeysJKyBFP5i-xvsNCuS-JwmiHsioDH90/edit?usp=sharing).
Nadat dat was gedaan had ik nog een creatieve sessie gedaan in mijn eentje omdat de creatieve sessie met [Shreyas, Meintje en Savannah](https://ikmens.gitlab.io/tysblog/post/summerschoolvijf/) niet nuttig was voor mij.
Deze sessie bracht mij op een concept uiteindelijk. <i>gelukkig</i> Na dat allemaal gedaan te hebben en uitgeschreven te hebben ben ik van start gegaan met schetsen van het prototype en een scenario die er bij ging. 
Dit duurde niet heel lang om te doen en daarna had ik er wireframes van gemaakt die ik toen ook had gebruikt om te kunnen testen.
Deze tests gaven mij genoeg informatie/feedback om zo te gaan werken aan mijn [high-fid prototype](https://xd.adobe.com/view/5086b6dd-9034-4a4b-5514-8c96e6ae6c5d-26b7/?fullscreen). 
De high fid was het einde van het project nadat ik wat mensen had laten gebruiken en de laatste fouten eruit had gehaald. 
Tot slot heb ik toen mijn STARRTs geschreven. Dit was een lang proces ook al moest ik maar 4 STARRTs schrijven. Het moeten wel de STARRTs zijn die mij het jaar kunnen laten halen. Dus hopelijk zijn ze dat ook.
<br />
<br />
All in all vond ik Summerschool wel een soort van leuk. Het was een compleet andere ervaring dan het rest van het jaar. Of misschien komt het omdat ik niet genoeg had geslapen de laatste paar dagen.
In ieder geval lijkt me het wel gaaf om simpelweg in een groepje van max. 3 personen te zitten. Ik weet dat ik zei dat het lastig is in [mijn post van kwartaal 2](https://ikmens.gitlab.io/tysblog/post/reflectiekwartaal2/) maar dat was omdat we gewoon nog niet genoeg skills hadden om zo te kunnen werken. 
Dat kan dus misschien de reden zijn waarom ik Summerschool een heel stuk leuker vond dan het rest van het jaar. Nu had je wat geleerd en had je één laatste moment om te laten zien wat je allemaal kan.
<br />
<br />
Hopelijk heb ik het jaar gehaald. Dus ik zeg alvast <b><i>Tot volgend jaar!</i></b>
<br />
<br />
<br />
<br />
<br />
<br />
<i>sorry Dyann dat ik niet meer posts heb gepost voor jou om te lezen maar ik had het echt druk</i> :P
