---
title: "Summerschool"
subtitle:  "Dag 5"
date:  2018-06-22
categories: ["School","Kwartaal5"]
tags: ["Summerschool"]
---
Het is alweer vrijdag! Heb mijn minimum behaald voor mijn enquête en wat creatieve sessies gedaan!
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Op de helft van Summerschool alweer. De enquête is beantwoord door 32 mensen! 
Iets meer dan de helft hiervan heeft een tour gevolgd en van de mensen die nooit een tour hebben gevolgd zouden ze er één volgen als deze interessant genoeg is.
Hier een rapportage voor maken en dan feedback hierover vragen zodat ik het kan gebruiken voor mijn <b><i>Onderzoeken Competentie</i></b>. Dat is dan één van de 4 klaar. 
Nu nog Ideevorming, Verbeelden&Uitwerken en Evalueren Ontwerpresultaten. Vandaag ga ik me voorbereiden op mijn creatieve sessie en er meedoen met 3(Shreyas, Savannah en Meintje) waar ik misschien zelf ook iets mee kan doen.
<br />
<br />
We hebben alle whiteboards genomen in de gang en onszelf afgesloten van de andere mensen. Zo konden we allemaal een whiteboard gebruiken om dingen op te schrijven voor onze creatieve sessie. 
<br />
<img src="https://i.imgur.com/3pOW4xM.jpg" width="50%" height="50%"/>
<br />
Ook ben ik begonenn met het kijken naar wat er is ingevuld op de enquête en nog wat deskresearch. Als ik hiervoor een Rapportage maak en feedback vraag kan ik beginnen aan mijn STARRT.
<br />
<br />
Korte post maar niet veel verschillende dingen gedaan vandaag. <b><i>Peace out!</i></b>