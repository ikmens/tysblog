---
title: "I'm done"
subtitle:  "Sort of"
date:  2020-01-13
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
Ik heb geaccepteerd dat ik niet elke dag kan bloggen. Ik heb het geprobeerd maar dit is al de 3e keer dat ik iets van een week niet heb gepost...
<!--more-->

<br />
<br />
<br />
<br />
De reden dat ik niet heb gepost is omdat ik bezig ben geweest met een aantal belangrijke dingen. Ik was bezig met mijn Criteria project afronden, met dev de comments updaten en last and certainly not least Stageverslag. 
Ik was gewoon zo bezig met deze dingen dat toen ik om 5 uur klaar was ook echt klaar was om naar huis te gaan en en te gaan slapen ofzo. 
Maar nu is mijn Stageverslag af. Ik ben verlost daarvan. <i>Hoop ik</i>. Zolang ik geen herkansing hoef te doen. Naast dit ben ik ook blij dat ik klaar ben met het Criteria project maar daar kon ik eerder mee klaar zijn geweest. 
Waarom vraag je je af. Dan had je mijn stageverslag maar moeten hebben gelezen :P 
<br />
<br />
<b><i>That's all for today. Peace out, Girl Scout!</i></b>