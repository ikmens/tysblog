---
title: "Let's do some reflecting"
subtitle:  "Reflectie semester 2"
date:  2019-07-05
categories: ["School","Year 2"]
tags: ["SemesterDos"]
---
Hierin kijk ik terug naar allerlei dingen die ik heb gedaan in semester 2. Lekker makkelijk in een blog post voor de mensen die niet meer willen lezen...
<!--more-->

<br />
<br />
<br />
<br />
Daar zijn we dan weer. Laatste blog post voor dit schooljaar. Raar het voelt alsof ik volgende week gewoon weer studio heb maar dat is niet zo. In ieder geval wat heb ik gedaan dit kwartaal??
<br />
<br />
Dit kwartaal was de design en deliver sprint. Hierin werd dus het concept gefinaliseerd en een prototype ervoor gemaakt. Er waren wat interessante momenten. 
Zoals ik het in een <a href="https://ikmens.gitlab.io/tysblog/post/iamconfussion/">blogpost</a> over had wou ik een feature testen voor het eind prototype maar zoals je misschien al door had tijdens het lezen van die blog post had ik nooit het woord feature gebruikt. 
Dat was omdat ik geen idee had over het woord,<i> naja ik kende het woord wel maar kwam niet bij me op toen</i>. En dit zorgde voor aardig wat verwarring tussen mij en me team. 
Gelukkig konden ik en me team het goed bespreken en kon ik een prototype gebruiken voor testen. Later was het woord wel besproken in de context van user testing en usability testing. 
Dit liet uiteindelijk een lampje in me branden en dat is nu ik erop terugkijk wel een soort leuk om te merken. Het geeft aan dat ik nu rustig maar zeker dingen ga doen die nuttig zijn om te doen maar nog niet ken. 
Ik ben begin een ontwerper blik/gedachtegang/etc. te ontwikkelen en daar ben ik wel blij om. Zeker omdat ik volgend jaar hopelijk op stage ga. <strike><i>Eerst nog eentje vinden</i></strike>
<br />
<br />
Wat nog meer mhmmmm.....Oh ja! Expo en eind prototype. Oh dat was maar even wat werk. Ik was de prototyper in het team dus in het begin kon ik wat dingen doen maar niet heel veel. 
Dat was een beetje saai maar ik heb tijdens die dingen ook nieuwe dingen geprobeerd. Maar nu was my time to shine. Dus ik had een XD prototype gemaakt. Boeien!!! who cares? Naja het prototype had 151 schermen omdat ik er voor wou zorgen dat de meeste dingen het zouden doen. 
151 schermen waarvoor velen kopieën waren van elkaar met een kleine verandering zodat een animatie er beter uit zou zien. Naja dat was de hoop maar dat is niet helemaal zo goed gegaan als je op mobiel kijkt. 
Dat is wel jammer omdat XD momenteel alleen via cloud documenten kan voor Android en alleen met Apple kan connecten. Dit zorgde ervoor dat heel veel renderen moest gedaan worden door een telefoon. En hierdoor ging het haperen en liep het prototype een heel stuk langzamer. 
Dat vind ik dus niet zo chill. Maar om eerlijk te zijn ik ben nog steeds trots op wat ik heb kunnen leveren. Volgende keer waarschijnlijk ook niet zo iets groots maken met XD. Misschien is het toch tijd om eens Android studio te gebruiken voor iets anders dan me Smartwatch. 
Of Invision, Axure, Framer. Whatever er is dit misschien beter zijn dan XD sinds XD nog een redelijk nieuw programma is. 
<br /> 
<br />
Als laatst dan nog de expo. Hoe ging die? Ik vond wel goed. Het was niet heel druk bij onze stand en dat was wel jammer maar ja wat doe je er aan. Betere ervaring creeëren de volgende keer! 
Het team heeft ook wel netjes hun best gedaan voor de expo en ik probeer ook niet anders te zeggen. Wat ik probeer te zeggen is dat ik de volgende keer een betere ervaring zou creëren om zo meer verkeer te brengen bij mijn stand. 
<br />
<br />
Als eindiger van deze belangrijke, all encompassing, blogpost. Het jaar was wel echt heel leuk. Het begin was een beetje moeilijk omdat ik in een andere klas was gezet dan de meeste van mijn eerste jaar genootjes en ik vind het nog steeds jammer dat ik ze niet zo vaak kon zien maar het heeft er wel voor gezorft dat ik meer mensen heb leren kennen. Daar ging ik voor. Dat is de reden waarom ik niet had gevraagd om terug geplaatst te worden. 
<br /> 
<br />
<b><i>Peace out! Girl Scout</i></b>