---
title: "New year"
subtitle:  "New me"
date:  2018-09-04
categories: ["School","Year 2"]
tags: ["SemesterUno"]
---
Het jaar is weer begonnen en mensen worden weer wakker. Soort van. Dit jaar zit ik in CMD2A op de 2de verdieping. Dit jaar ben ik ook peercoach!
<!--more-->

<br />
<br />
<br />
<br />
<br />
Met een nieuw jaar hoort ook een nieuwe opdracht en opdrachtgever natuurlijk. Dit semester is de opdrachtgever dan ook Woonstad Rotterdam met als opdracht 
<br />
<br />
<i>Hoe kan Woonstad Rotterdam het face-to-face klantcontact venieuwen bij ons op kantoor of in de wijk.</i>. 
<br /> 
<br />
We gaan met deze opdracht aan de slag met sprints en de eerste sprint(en huidige) is helemaal in het doel van onderzoeken ook wel <b><i>Discovery fase</i></b> genoemd. 
Deze fase word dan gedaan met je team en om erachter te komen wat mijn team was dit kwartaal had ik samen met andere teamleiders random kaartjes gevonden met rollen erop om zo een team samen te stellen.
Mijn team bestaat nu 4 mensen inclusief ik:
<br />
<br />
<ul>
<li>Hanno</li>
<li>Verena</li>
<li>Sico</li>
<li>Tyoni(ik)</li>
</ul>
<br />
<br />
In het team hebben we 2 visual designers. Een met focus op prototyping en de andere met focus op concepting. Ook hebben we een concepter en een interaction designer. 
Helaas vandaag niet iedereen er waardoor ik niet iedereen kan ontmoeten en ik ben er niet voor de volgende 3 dagen. Dus dan kan ik pas iedereen ontmoeten volgende week dinsdag.
We moesten ook nog een team uitje doen voor volgende week dinsdag maar dat gaat niet lukken sinds ik op intro kamp ben met de eerste jaars CMD studenten.
Dus voor nu hebben we gepland om even op te schrijven wat we weten van de opdracht en onze eigen interpretatie ervan zodat we kunnen zien hoe ver iederen is en als we allemaal op één lijn zitten.
We shall see how it goes!