---
title: "It is like that huh"
subtitle:  "I'm getting somewhere"
date:  2019-10-11
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
So as follow up on yesterday I got feedback on my redesigns...
<!--more-->

<br />
<br />
<br />
<br />
Yesterday I had a brainstorm with my instructor (i think is what the word is in english) and we concluded that some extra things have to be taken into account to make sure everything is future proof. 
And today I got some feedback on the designs I created with that idea in mind and it worked starts to fall together. The general look of what I'm working on is basically determined and now I'm working out spacings and how it will fall in line with the things it comes in contact with. 
This part even though it sounds small is actually quite a big thing. It involves alot of creating alot of rules and making sure those rules are consistent in more than 1 look. 
Making sure that when those rules are applied they don't interfere with the user's overall work flow. And when all of this has been done and I don't have any other things I forgot I can start testing! 
Then the real fun begins and I can start hearing from non-designers/developers why this won't work for them. Which hopefully won't happen haha. That's all for today.
<br />
<br />
<b><i>Peace Out. Girl Scout!</i></b>