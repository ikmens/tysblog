---
title: "Oefenen met tekenen"
subtitle:  "van mensen!"
date:  2018-01-13
categories: ["Personal progression"]
tags: ["Blog"]
---
Ik ben begonnen met het oefenen van mensen tekenen. Dit doe ik zodat ik makkelijker storyboards kan tekenen en omdat ik het wel leuk vind om te doen.
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Ik teken ze in "manga" style want dat vind ik het leukst eruit zien en het is makkelijk. 
Ik gebruik voor het tekenen een HB potlood een 2,5mmx5mm gummetje en een klein boekje met dotted en lined pages. 
Dotted gebruik ik dan voor tekenen en lined kan ik notities over de tekeningen opschrijven.
&nbsp;
&nbsp;
![](https://i.imgur.com/HxIXqlI.jpg)
![](https://i.imgur.com/PuSHLFH.jpg)
![](https://i.imgur.com/7fFW3UM.jpg)
![](https://i.imgur.com/l4uHBWA.jpg)