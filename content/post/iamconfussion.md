---
title: "Interessant moment"
subtitle:  "Misscommunicatie & Multimedia Design"
date:  2019-05-13
categories: ["School","Year 2"]
tags: ["SemesterDos"]
---
De ontwerper betrekt de samenwerkingspartners bij het ontwerproces door feedback te vragen. 
<!--more-->

<br />
<br />
<br />
Er is dus iets interessant gebeurd gisteravond. Gister rond 6-en had ik een klein prototype gemaakt met XD die, van mijn perspectief, het concept kort uitlegt. 
Het zijn 5 schermen die laten zien dat de app een notificatie laten zien, het artikel  met video laten zien en vervolgens de lezer de mogelijkheid geven om het volledige artikel te lezen wanneer ze willen. 
Dit had ik eerst geschetst in mijn schriftje met wat tekst zodat ik wist hoe het ze gang zou gaan. Voor de zekerheid nog aan iemand in me team laten zien en het hier met ze over gehad. 
<br /> 
<br />
<img src="https://i.imgur.com/9eN3y0a.jpg?1" width="50%" height="50%">
<br />
<br />
Geen problemen niks. Ik dus leuk maken thuis. Duurde niet heel lang sinds ik al wat ervaring heb met XD en al wist wat ik moest maken. 
Ik stuur dus een gifje naar mijn team toe van wat ik ff snel in elkaar heb gezet om mogelijk te gebruiken voor testen of dergelijke. Plotseling hoor ik van 2 teamleden dat zij een ander beeld hebben van het concept. 
But how!? En ik ben nu een beetje verward over wat er fout is gegaan en wat ik ga doen met het prototype. Anyway wat er ook gebeurd hier heb je de gif. 
<br />
<br />
<video width="50%" height="50%" controls>
<source src="https://i.imgur.com/PpizSZV.mp4" type="video/mp4"/>
</video>
<br />
<br />
<b><i>Peace out! Girl Scout</i></b>