---
title: "Brainstorming time"
subtitle:  "Beetje Engels, Little Dutch"
date:  2019-10-09
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
Wednesday middle of the week, cycling and some more...
<!--more-->

<br />
<br />
<br />
<br />
Dus vandaag eerste keer van Amsterdam Zuid naar werk gefietst. Dat was een ervaring die ik niet nog eens wil mee maken tbh. Zadel is iets te ongemakkelijk maar het de omgeving is wel cool. 
Maar dat is niet zo belangrijk. Het belangrijkste dat gebeurd is vandaag is een meeting die ik had met Esther (mijn stagebegeleidster). Meer een brainstorm! We gingen kijken hoe haar project samenvalt met die van mij.
We hadden gekeken naar wat er overlapt zoals ik net zei maar ook naar de ergste gevallen en toekomst-proofing want dan is het geen probleem nu of in de toekomst. Dat was heel gaaf en was zeker even puzzelen. 
Omdat we echt moesten gaan kijken naar de uiterste gevallen en daarin moesten voorkomen dat iets fout gaat in zulke momenten. Meer heb ik niet te melden behalve dat ik bezig ben om de VR race setup te fixen in de college dus dat zou gaaf zijn. 
<br />
<br />
That's all folks!
<br />
<b><i>Peace out. Girl Scout!</i></b>