---
title: "Nog een design challenge"
subtitle:  "Adobe xd creative challenge"
date:  2019-05-13
categories: ["School","Year 2"]
tags: ["FreeTime"]
---
Bezig geweest met Adobe XD. Eerste keer dat ik deze challenges ga volgen dus laten we hopen dat ik ze allemaal doe...
<!--more-->

<br />
<br />
<br />
<a href="https://www.behance.net/dailycreativechallenge" target="_blank">Adobe XD Daily Creative Challenge</a>. Basically elke dag een challenge waarmee je werkt met XD. 
Tijdens deze dagen geven ze je tips, feedback etc op hoe je te werk kan gaan met XD en geven ze je tools en links naar informatie die verder kunnen helpen. 
Tot nu toe is dit de derde keer dat ik met XD heb gewerkt en het is super interessant en ik leer er telkens meer van. Challenge 1 was creeër een splash en landing page voor een "movie app". 
Hier heb je wat ik tot nu toe heb gemaakt. 
<br />
<br />
<video width="50%" height="50%" controls>
<source src="https://i.imgur.com/bdtVesG.mp4" type="video/mp4"/>
</video>
<br />
<br />
Tot nu toe heb ik er wat feedback op gekregen van andere mensen die deze challenge doen en het heeft me al wat nieuwe dingen geleerd. Visueel gezien hebben ze me geleerd over "Baseliens". 
Voor een uitleg die het goed uitleg zal ik je door sturen naar deze video --> <a href="https://www.youtube.com/watch?v=-Kp66bBZoy8&t=24s" target="_blank">Using baseline grids in web & UI design</a>
<br />
<br /> 
<b><i>Peace out! Girl Scout</i></b>