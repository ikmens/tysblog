---
title: "Different perspective"
subtitle:  "Let's try looking from your side"
date:  2019-10-15
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
Begeleidster helaas ziek dan maar even doorwerken...
<!--more-->

<br />
<br />
<br />
<br />
Dus ja vandaag was het plan om een "Design Review" te doen. Dus hierin zou ik met mijn begeleidster op één lijn komen over hoe alles eruit gaat zien en als we niet te ver van elkaar aflopen. 
Basically aligning. Helaas is mijn begeleidster ziek vandaag dus kon dat niet gebeuren. Ben voor deze reden maar eens kritisch naar mijn eigen werk gaan kijken. <i>Natuurlijk doe ik dat al tijdens het ontwerpen/schetsen/whatever maar nu heel kritisch</i>. 
Ik heb dan ook tijdens dit moment geschreven wat ik vond van mijn ontwerpen, wat ik er goed aan vond en waarom, wat ik er slecht aan vond en waarom, daarna gekeken naar wat ik misschien kan combineren en daarna een klein plannetje opgesteld voor wat ik de rest van de dag kan gaan doen. 
Super intens dus. Hopelijk is mijn begeleidster er morgen wel weer maar ik denk het niet dus dan ga ik nog iets verder werken aan mijn ontwerpen en dan hopelijk donderdag een review doen. Anyhow...
<br />
<br />
<b><i>Peace Out. Girl Scout!</i></b>