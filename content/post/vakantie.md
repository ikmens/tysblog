---
title: "Meetup"
subtitle:  "Tea!"
date:  2018-05-02
categories: ["School","Kwartaal4"]
tags: ["Blog","School"]
---
Het is vakantie! We hadden besloten om in de vakantie bij elkaar te komen om te bespreken wat er allemaal moest gedaan worden naar mate van onderzoek en brainstormen.
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Dus vandaag hebben we afgesproken bij [Tealab](https://thetealab.nl/) om daar wat te drinken en school werk te doen. Voor deze dag hadden we ook afgesproken wat deskresearch te doen zodat we misschien wat dingen daarvan kunnen meenenemen.
Sommige mensen waren het helaas vergeten of hadden niet veel gevonden maar dat stopte ons niet van het daar in tealab het te zoeken. Dus dat hebben we ook gedaan.
<br />
<img src="https://i.imgur.com/qQueXBg.jpg"/>
<br />
Na dit onderzoek wouden we gaan brainstormen voor een concept maar we realiseerde ons dat we nog niet genoeg informatie hadden om een concept te verzinnen dat het probleem zou verhelpen.
Dus na daarachter te komen zijn hadden we besloten nog een keer onderzoek te gaan doen in Afrikaanderwijk alleen dit keer beter omdat de eerste keer niet zo goed ging.
Onderzoeksplan opgesteld en een paar vragen voor een interview. Die alleen duurde vrij lang dus na dat zijn we allemaal naar huis gegaan om hebben we genoten van onze 2de helft van de vakantie.