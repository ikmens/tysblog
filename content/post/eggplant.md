---
title: "Short day"
subtitle:  "Almost weekend"
date:  2019-10-04
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
Halve dag! Er word op mij getest!
<!--more-->

<br />
<br />
<br />
<br />  
Halve dag vandaag omdat Michelle aan me had gevraagd om wat dingen te testen op mij dus vandaag heb ik niet heel veel gedaan. 
Maar vandaag heb ik wel wat voortgang gemaakt op mijn project en dat is nice. Nu moet ik gaan kijken naar flow. Vervolgens had ik mijn weekly checkin! Hierin praat ik met mijn begleider over hoe het gaat, wat zij wou vragen en/of ik nog vragen heb. 
Voornamelijk gehad over leerdoelen en de planning daarover. We hebben afgesproken om maandag samen mijn planning te maken voor de volgende 2 weken dus dat is cool! Krijg ik wat meer inzicht in hoe zij dat zou doen. 
Deze soort planning is iets dat ik ook voor school kan gaan gebruiken. Hier post ik volgende week wat meer over misschien met wat screenshots.
Anyhowwwwww
<br />
<br />
<b><i>Peace out. Girl Scout!</i></b>