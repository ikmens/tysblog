---
title: "Fresh sprint"
subtitle:  "planning to run"
date:  2019-10-07
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
It's monday yayyyyyyyyyyy. Today was all about making a planning and working on making components better...
<!--more-->

<br />
<br />
<br />
Today was my first time participating in an actual sprint planning so that was awesome! In this sprint planning they talk about a lot of different things. Examples are:
<ul>
    <li>A reflection of the past 2 weeks</li>
    <li>Achievements someone wants to talk about</li>
    <li>Planning for the next 2 weeks(sprint)</li>
</ul>
<br />
So that means I also made my own sprint planning for the coming 2 weeks. 80 hours are now planned in and it is basically 70 hours of working on my project which seems so insane!
8 hours every day for the project feels like I am working really slow but I don't think I am. So that's something to pay attention to. Most of my time today went into working on making components which was driving me mental. 
Basically it's working on parts of the prototype that frequently come back in the prototype and it will save me time to just plop them in there instead of remaking them every time.
That's what I did today tomorrow I have a busy schedule and my Swapfiets gets delivered! So then I can cycle to the metro instead of use the tram. I'm not sure how much I can cycle anymore during this year but hopefully I can for most of the time.
<br />
<br />
<b><i>Peace out. Girl Scout!</i></b>