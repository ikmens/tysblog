---
title: "Reflectie Semester2 0,5"
subtitle:  "Origami Reflect"
date:  2019-04-15
categories: ["School","Year 2"]
tags: ["SemesterDos"]
---
Recap tijd! Laten we eens terugkijken op wat er allemaal is gebeurd dit kwartaal en (mogelijk) hoe ik dat volgend kwartaal beter ga doen...
<!--more-->

<br />
<br />
<br />
<br />
Om dat maar gelijk te hebben gezegd. Ik heb niet veel geblogd (geblogt idk) dit kwartaal en vind dat dit volgend kwartaal wel wat beter kan. Hierdoor mis ik een beetje wat ik het begin heb gedaan voordat ik naar Berlijn ben geweest. 
Dus laten we over gaan naar Berlijn!!!! 
<br />
<br />
Berlijn. 5 dagen en het was echt geweldig. First off waarom was ik in Berlijn? Ik was in Berlijn met de 3de jaars en een paar 2e jaars. Tijdens deze reis ging we langs meerdere design studios en konden we zien hoe ze te werk gingen en konden we nog vragen stellen. 
Iedereen mog langs 3 bedrijven gaan. Ik had ervoor gekozen om langs ART+COM, AJ&Smart en Native Instruments. Mijn favoriete van de 3 was AJ&Smart en daarom ga ik alleen daarover schrijven in deze reflectie. 
AJ&Smart is een klein bedrijf met iets minder dan 30 mensen. Ze werken heel erg close met hun opdrachtgevers om zo een product te leveren waar iedereen tevreden mee is. 
Soms gebruiken een techniek/Jam genaamd <i>"Lightning Decision Jam"</i> hierin doen ze kort in een week concepten en prototypen. Met deze techniek hebben ze dan in ongeveer 6 dagen een high-fid prototype dat ze kunnen laten zien aan opdrachtgever en doelgroep voor testen. 
Met nog een dag extra voor enige fixes. Dit doen ze om de opdrachtgever een idee te geven van hoe het er uit zal zien. Voor ons hadden ze ook een boekje gemaakt die we mochten mee nemen om zelf uit te proberen. Ik heb het nog niet volledig uit geprobeerd maar wel bepaalde onderdelen ervan. 
Bijv. How Might We heb ik gebruikt met mijn team en daardoor hadden we al een idee dat we iets met tijd wouden gaan doen en dat is uiteindelijk uitgebloeid tot een concept waar de opdrachtgever geïnteresseerd in is. 
Om terug te komen over Berlijn. Voor de rest ben ik nog naar <a href="https://urban-nation.com/" target="_blank">Urban Nation</a> gegaan. Een museum waar street artists hun kunst kunnen laten zien. Het is echt heel indrukwekkend. 
Het was super gaaf en zou er zeker weer langs gaan als ik weer ooit in Berlijn ben. 
<br />
<br />
Wat is er na Berlijn gebeurd? Genoeg. Ik heb veel fieldresearch gemist helaas maar mijn team is gelukkig hard door gegaan daarmee. Ik moet dus volgend kwartaal nog iets gaan fieldresearchen mogelijk prototype testen ofzo. 
Maar met alle info van de fieldresearch had ik nog kunnen helpen met een research wall maken en met brainstormen. Oh ja!, <a href="https://ikmens.gitlab.io/tysblog/post/prebrainstorm/" target="_blank">De lotus blossum techniek geprobeerd.</a> <i><- Voor meer info daarover lees de blog hiervoor!</i> 
Tijdens het gebruik van deze techniek zijn we op een paar concepten gekomen dus ik vind wel dat het voor herhaling vatbaar is. De concepten hebben we toen gepresenteerd aan de opdrachtgever maar niet voordat we een conceptposter of dergelijke ervoor hadden gemaakt. Ik beslootte om een animatie te maken met after effects om één van de concepten te laten zien. 
Maar Tyoni waarom zoveel moeite? Dat deed ik omdat ik snel naar een high-fid versie wil gaan om zo een soort van de techniek van AJ&Smart te gebruiken. Dit liet ook wat beter zien wat ons concept is vind ik zelf. Marije en de opdrachtgever vonden het ook wel interessant. 
Wel vroeg de opdrachtgever over dit het eind blik is of een tussenstap. Het was meer een tussenstap. Een veiligere optie vonden zij ook maar volgende zou hij liever willen zien hoe het er echt als eind eruit zal zien dus dat is ook iets dat ik ga doen voor het einde concept. 
<br />
<br />
Voor de rest zijn we vooral bezig geweest met productbeoordeling gesprek en momenteel bezig met leerdossier. De productbeoordeling ging voor mijn gevoel wel goed en wat ik hoorde van mijn team ging het voor hun gevoel ook wel goed. 
Dus ja dat is het voor nu!
<br />
<br />
<b><i>Peace out! Girl scout</i></b>