---
title: "Into another reality"
subtitle:  "Meeting, planning, cycling and reality changing"
date:  2019-10-08
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
Not much work to be honest...
<!--more-->

<br />
<br />
<br />
Not a really productive day today. Not in terms of work here at FBF. I worked on some school things actually. Today I worked on creating a slack for interns of my school year. 
I created it because I noticed that it is hard for us (interns) to reach out to one another and to find out where everybody works. Setting this up and getting people their email took way longer than I expected it to happen. 
Besides this I also joined in on a sort of meeting, more an alignment to see where we (the senior designer and I) are and if we are still going the right way with our work and don't stray too far away from the scope. 
It was my first time and it was really cool to participate in it because I had something to show and "align". My project was a bit small for my feeling at the start but the longer I get along in the project it starts to become larger and larger which is really interesting and amazing because it shows the impact I will have on the program during my internship. 
They really see me as someone who works here! Great! 
<br />
<br />
<i>After all this I decided to try and fix the VR setup in the college at our workplace. And I fixed it!!!! AAAaaaaaaaaaaaawesome!! Beat saber for the win.</i>
<br />
<br />
<b><i>Peace out. Girl Scout!</i></b>