---
title: "Kill your darlings"
subtitle: "Rip app"
date: 2017-09-19
categories: ["Personal progression","Kwartaal1","School"]
tags: ["Blog"]
---
RIP onze app. We kregen vandaag te horen dat we een nieuw concept moeten gaan maken voor onze app omdat we zo verder de creatieve diepte in kunnen gaan. Hoewel het jammer is dat we overnieuw moeten beginnen is het ook wel fijn want hierdoor kunnen we wat andere ideeën gaan achtervolgen.
