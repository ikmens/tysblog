---
title: "Totem en Festival map Illustrator"
subtitle:  ""
date:  2017-11-02
categories: ["Personal progression","Kwartaal1"]
tags: ["Blog","School"]
---

Illustrator les laatste opdracht festival maken met lagenda
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Eerst tekenen zodat ik een layout kan verzinnen en daarna op illustrator uitwerken.
Naar huis gegaan en op illustrator het laatste dier gemaakt voor de totem en ingeleverd.
&nbsp; 
![totem](https://i.imgur.com/RLKmKlt.jpg) 
![festivalmap](https://i.imgur.com/fCem6yq.jpg)