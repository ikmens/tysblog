---
title: "Nieuw kwartaal!"
subtitle:  "New team New start"
date:  2017-11-22
categories: ["Personal progression","Kwartaal2"]
tags: ["Blog","School"]
---
Best een tijd geweest tussen posts omdat er niet veel is gebeurd maar
wat er is gebeurt kan ik in één post zetten. 
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Vorige week dinsdag(14-11-2017): Naar het paard van troje geweest met alle CMD-1 studenten voor onze
briefing. Wij moeten een clickbaar prototype voor hen maken die zal zorgen voor meer vaste klanten. Bij deze opdracht hebben wij allemaal verschillende doelgroepen gekregen.
Mijn groepje en ik(That 70s Team) hebben de doelgroep "De Oldskool"(~>38 jaar) gekregen. Deze gaan wij interviewen en dergelijke maar voordat gaan wij een merkanalyses maken.
Deze merkanalyses zullen over het Paard, Fabrique en 3 concurrenten(Melkweg, Pathé en Maassilo) gaan zodat wij een duidelijk beeld hebben van onze opdrachtgevers en over wat andere
bedrijven/stichtingen anders doen dan het Paard.

Met dit kwartaal zal ik een heel stuk verder komen met mijn competenties. Misschien wel één of meer behalen als ik genoeg bewijs bewaar wat ik jammer genoeg vorig kwartaal niet had gedaan.
Ik ga mij focussen op genoeg bewijs bewaren voor de STARRTs en op posten.

![groep](https://i.imgur.com/PNxwB4M.jpg)