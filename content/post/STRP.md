---
title: "Golden Stripes"
subtitle:  "Lights are cool"
date:  2019-04-02
categories: ["School","Year 2"]
tags: ["SemesterDos"]
---
Begonnen met "Golden Circle" en naar STRP festival geweest in Eindhoven...
<!--more-->

<br />
<br />
<br />
<br />
Dus laten we beginnen bij het begin. De Golden Circle is een manier om de waarde van een bedrijf, product of service te bepalen. Dit zou dan gebeuren doormiddel van 3 vragen die je beantwoord. 
Deze 3 vragen zijn:
<br />
<br />
<ul>
	<li>Why</li>
	Waarom doe je het? Wat is jullie doel?
<br />
<br />
	<li>How</li>
	Hoe doen jullie het? Welke technieken of waardes gebruiken jullie voor de realisatie?
<br />
<br />
	<li>What</li>
	Wat bieden jullie? Wat doen jullie?
<br />
<br />
</ul>
<br />
<br />
Met deze 3 vragen beantwoord zou je gemakkelijk kunnen uitleggen wat je concept is of waar het binnenvalt. Voor ons was het moeilijk om te doen omdat ik en Sanne verschillende beelden hadden van wat Golden Circles inhouden. Ik zag het als een meer algemeen ding waar onder alle concepten onder zouden vallen. Zij zag het als iets dat voor elk concept word gemaakt. We zaten dus even te stoeien met wat we gingen zetten bij Why. Mijn Why's momenteel zijn: 
<br />
<br />
<i>
<ul>
    <li>We willen ervoor zorgen dat lezers zonder problemen het nieuws kunnen lezen dat ze willen lezen zonder in een filterbubbel te komen</li>
    <li>Wij geloven dat mensen nog geen goede transparante personalisatie hebben gehad en dat we deze kunnen bieden</li>
    <li>Wij willen een persoonlijke ervaring van het nieuws lezen creeëren die ruimte houd voor serendipiteit</li>
</ul>
</i>
<br />
Hier was het een beetje bij gebleven en hadden we Ellen(studio docent) gevraagd om ons te helpen. Zij zag het probleem en zei dat we beiden gelijk hadden. Een Golden circle kan zo gebruikt worden zodat meerdere concepten in vallen maar er kunnen ook GC's worden gemaakt die alleen voor dat concept zijn.
Dus toen vroeg zo over onze concepten en had ik uitgelegd over 1 van onze concepten. <b><i>Patch notes</i></b>. Long story short het personaliseert je nieuws feed tot een extend maar met volledige transparantie. Het laat dus zien op welke berichten het is gebaseerd en je kan zelfs aangeven of je een nieuws artikel wilt gebruiken in de personalisatie of niet.
In ieder geval dus ze hielp ons met de golden circle voor dit concept. Basically:
<br />
<br />
<ul>
    <li>Why?</li>
    Wij geloven dat mensen doormiddel van transparantie personalisatie gemakkelijker zullen accepteren. 
    <br />
    <li>How?</li>
    Doormiddel van uiterste transparantie
    <br />
    <li>What?</li>
    Patch's notes
    <br />
</ul>
Vervolgens ben ik met Dyann, Sico en Jiska naar <a href="https://strp.nl/?gclid=EAIaIQobChMIhYzNz8u04QIVguFRCh1ZnQKaEAAYASAAEgIK4PD_BwE">STRP festival</a> gegaan tijdens CT lab. 
Misschien wel mijn beste keuze tot nu toe. Super gave ervaring alles was zo cool. Ugh ik ben nog steeds zo overwelmd door de awesomeness van alles. Definitely a recommend. 
Anyhow.....
<br />
<br />
Peace Out. Girl Scout.

