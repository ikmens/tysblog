---
title: "Maastricht!"
subtitle:  "Trip!"
date:  2018-02-09
categories: ["School","Kwartaal3"]
tags: ["Blog","School"]
---
Maastricht zit er op! Het was leuk. Ik heb nieuwe mensen leren kennen, mooie gebouwen gezien en karaoke gedaan.
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
<ul>
<li> <b>Dag 1: Arrival</b></li>
We zijn daar aangekomen, hebben onze kamersleuters gekregen en zijn begonnen met groepjes maken. <br />
Ik zit in een kamer met Sem, Maurits, Tim en Jip.<br />
De groepjes zijn compleet random gemaakt en ik zit met 4 mensen waarvan ik er 1 een klein beetje ken omdat hij bij mij in de klas zit.
We hebben een doelgroep gekregen "Jonge ouders" 25-30 jaar oud. Ons doel is om ze bewuster te maken van hun gezondheid.
In de avond heb ik met wat mensen karaoke gedaan en heb ik zelfs samen met andere Bohemian Rhapsody gezongen.<i> zover je het zingen kon noemen</i>
<br />
<br />
<li> <b>Dag 2: Reasearch en Prototype</b></li>
Deze dag moesten we interviews doen en een prototype maken. We hadden 2 mensen geinterviewt omdat het vrij lastig was om op een donderdag middag 25-30 jarigen ergens te vinden.
Met de informatie van deze 2 mensen en wat deskresearch hadden we een concept verzonnen voor een Welness/Daycare center.
Deze hadden we toen uitgewerkt met 2 kamers.
<br />
<img src="https://i.imgur.com/BGOtGWZ.jpg" alt="NeeNu" width="128" height="128">
<br />
We hadden daarna de expo waarin peercoaches en docenten vragen konden stellen over het concept/prototype. 
Helaas hadden we niet gewonnen maar hebben we wel plezier gehad.
In de avond heb ik met vrienden rond gehangen in het hostel tot dat ik ging slapen.
<br />
<br />
<li><b> Dag 3: Leaving</b></li>
Op deze dag is er niet veel meer gebeurd behalve dat Sem een yoga les gaf aan 2 mensen. 
We hebben onze spullen gepakt en zijn in de bus terug naar huis gegaan.




