---
title: "The calm before the storm Part 1"
subtitle:  "It's so peaceful...too peaceful"
date:  2019-04-01
categories: ["School","Year 2"]
tags: ["SemesterDos"]
---
Great minds think alike meestal laten we hopen deze keer niet...
<!--more--> 

<br />
<br />
<br />
<br />
Het is weer maandag! Ik dacht laat ik eens een blog schrijven voordat er een creatieve sessie is gebeurd zodat ik mijn "hypothese" echt super legit is. 
Dus in nu is het 09:02 terwijl ik dit typ. Om 11 uur heb ik afgesproken met mijn team en hebben we 2 uurtjes om wat sessies te doen om wat concepten te verzinnen. Voor dit moment was iedereen gevraagd om een creatieve techniek te vinden die ze willen gaan gebruiken dus ik ook! 
Ik heb er 3 die ik wil gaan doen zodat ik er nog twee als back-up heb als de andere door iemand anders word gekozen. Deze drie zijn:
<br />
<br />
<ul>
    <li>Time travel</li>
Hoe zou het gaan in de toekomst? Neem een later stuk in tijd en ga met je team verzinnen hoe in de toekomst dit probleem zou opgelost zijn.
<br />
<br />
    <li>Niche</li>
Kies een specifieke doelgroep en kijken hoe je het zou oplossen voor alleen hen. Vb. Hoe zou je personalisatie, waar geen serendipiteit aan kwijt gaat, voor tieners kunnen inplementeren die alleen tijd hebben tijdens schooluren.
<br />
<br />
    <li><a href="https://www.backelite.com/2018/02/lotus-blossom-brainstorming-technique/" target="_blank">Lotus Blossum</a></li>
Deze is iets lastiger uit te leggen en daarom heb ik de link er bij toegevoegd maar om het te proberen. Bij deze techniek is er een "Core concept/woord" bij dit concept/woord verzinnen teams tot en met 8 associaties. 
Deze 8 associaties worden dan iets verder gelegd en bij deze associaties probeert het team dan ideeën te verzinnen zonder de "Core" in gedachte te hebben.
<br />
<br />
</ul>
Zelf denk ik dat de "Time travel" brainstorm techniek iets beter gaat werken omdat ik deze wel eens eerder heb gebruikt maar ik ben ook benieuwd hoe effectief de andere 2 zouden zijn. Voor deze reden denk ik dat ik de Lotus ga proberen met mijn team en als het niet lukt..jammer! Weet ik dat weer voor volgende keer. 
Voor de rest heb ik niks om over te schrijven voor nu dus tot later dan maar!
<br />
<br />
Peace out Girl Scout!
