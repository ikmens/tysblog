---
title: "Nieuw Team!"
subtitle:  "Laaste project"
date:  2018-02-13
categories: ["School","Kwartaal3"]
tags: ["Blog","School"]
---
Nieuwe groepjes gemaakt voor het laaste project dit jaar. Dit project zal 2 kwartalen duren inplaats van 1 kwartaal zoals de eerste 2 keer was.
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Nu zullen we dus meer tijd hebben voor onderzoeken en testen want de eerste twee kwartalen was dat vrij lastig om te doen.
Het nieuwe groepje is :
<br />
<ul>
<li> Sophie <b>Concepter</b></li>
<li> Savannah <b>Onderzoeker</b></li>
<li> Jari <b>Interaction designer</b></li>
<li> Meintje <b>Visual Designer</b></li>
<li> Tyoni(ik) <b>Teamcaptain</b></li>
</ul>
<br />
Tijdens dit project zal ik mijn best doen om een zo goed mogelijk teamcaptain zijn en mijn competenties behalen.
