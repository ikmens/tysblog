---
title: "Presentation!"
subtitle:  "Watch me watch me"
date:  2018-04-20
categories: ["School","Kwartaal4"]
tags: ["Blog","School"]
---
Het is vrijdagochtend en we moeten gaan presenteren! In deze presentatie praten we over onze plan van aanpak. We moeten hierin duidelijk maken aan de docenten wat wij als team willen gaan doen dit kwartaal en hoe we dit gaan doen.
Deze presentatie werd gegeven door mij en Meintje. We wouden hierin vooral praten over de huidige situatie sinds dat ons het meeste interesseerde. 
Meintje en ik hadden samen opgesteld wat er in de presentatie zal komen en ik had de powerpoint gemaakt. 
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Na de presentatie hadden we feedback gekregen van de docenten en van de groep die er ook bij was. We kregen als feedback dat we niet duidelijk maakte hoe we te werk zouden gaan en dat we niet duidelijk een conclusie hadden getrokken uit eens onderzoek. Maar het onderzoek dat we hadden gedaan was goed.
Hoewel we wel hadden uitgewerkt hoe we dingen aan gingen pakken hadden we er niet over nagedacht om dit in de presentatie te zetten sinds het dan lang zou duren. 
Dat weten we dan voor de volgende keer en zal ik zeker meenemen naar mijn volgende presentatie.
<br />
<br />
<img src="https://i.imgur.com/B6SBuhp.png" width="50%" height="50%"/>
<br />
<img src="https://i.imgur.com/9tO3OlR.png" width="50%" height="50%"/>
<br />
<img src="https://i.imgur.com/kyJW84B.png" width="50%" height="50%"/>