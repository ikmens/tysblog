---
title: "Sales call"
subtitle:  "The early birds catches the worm"
date:  2019-10-03
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
Lekker vroeg opgestaan om bij de sales call te zijn
<!--more-->

<br />
<br />
<br />
<br />
Dus ja lekker om 5 uur opgestaan vandaag om op 8 uur op kantoor te zijn omdat ik bij de sales call wou zijn. Super gaaf om te zien hoe het sales team te gang gaat met klanten en klanten krijgen. 
Zo kwam ik ook wat meer achter de waardes van het bedrijf en hun mindset naar het leer stelsel. Wel een beetje moe erna maar moet even kunnen. 
<br />
<br />
Vervolgens ben ik verder gegaan met mijn project dus zitten schetsen en kloten in Figma. Dit heeft het meest van mijn tijd ingenomen. Buiten dat ben ik gaan kijken naar hoe mijn planning er uit gaat zien voor de volgende 2 weken. 
Maar dat is alles voor nu.
<br />
<br />
<b><i>Peace out. Girl Scout!</i></b>