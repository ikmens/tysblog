---
title: "Summerschool"
subtitle:  "Dag 3"
date:  2018-06-20
categories: ["School","Kwartaal5"]
tags: ["Summerschool"]
---
Vandaag naar Den Haag gegaan. Het is een hele mooie plek zeker om te gaan fietsen!
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Dag 3 alweer van Summerschool. Tijd gaat snel dus daarom ben ik vandaag vroeg op school gekomen om aan mijn onderzoeksplan te werken zodat ik deze dezelfde dag nog kan uitvoeren.
Helaas kon ik geen feedback vragen sinds Bob de enige was die er was en hij is de hele tijd bezig! Dan maar morgen feedback vragen. Ik ga naar Den Haag met Shreyas en Prewish. 
We gaan kijken naar waar we de huursystemen zien en hoeveel mensen er van afweten in Den Haag.
<br />
<br />
We zijn aangekomen bij Den Haag HS. Als allereerst daar gezocht naar de fietsen stalling. Lastig te vinden maar na een tijdje gevonden. Bij de fietsenstalling is ook een miezerig klein bordje waarop staat OVFiets!.
Mhmm, lijkt erop alsof ze niet echt willen dat mensen weten dat het bestaat. Voor de rest hebben we geen gehuurde fietsen gezien dus gaan we maar verder lopen.
Een heel klein stukje verder van Den Haag HS is de [Haagse Hogeschool](https://www.dehaagsehogeschool.nl/). Prachtige school maar we zijn er niet voor sightseeing.
Hier ook geen huurfietsen gezien vaag genoeg. Zouden ze misschien iets verder liggen of zo goed als niet gebruikt worden door studenten daar omdat HS zo dichtbij ligt. Waarschijnlijk het laatste.
We gaan maar weer verder richting Den Haag Centraal omdat we niet veel tijd hebben. Onderweg daarnaar toe ook niet veel huurfietsen gezien dus we zijn van plan even ergens te gaan zitten en te observeren.
Na ongeveer een half uurtje hebben we zo'n 113 fietsen gezien waarvan misschien 1 of 2 huurfietsen. Misschien is het niet zo populair hier of worden ze niet zo vaak gebruikt door mensen die hier wonen.
We gaan dus weer verder naar DHC(Den Haag Centraal) en gaan wat interviews plegen. Ik vroeg mij af waar de OVFietsen waarin op DHC dus ik vond dat de perfecte openingszin om interview te starten met wat mensen.
Ik vraag het aan 2 meiden die NRC kranten aan het uitdelen zijn. We wisten gelijk waar ze waren na dat vroeg ik ze nog meer over huur fietsen en als ze die zelf gebruiken. 
Ze gebruiken ze zelf ook! maar ze komen niet uit Den Haag dus helaas. Prewish en Shreyas hadden ook nog mensen geïnterviewd voor dat we die dag weer naar huis gingen.
<br />
<br />
Ik ben niet veel extra's te weten gekomen voor een concept maar wel meer over concurrenten dus dat kan wel handig zijn voor later. <b><i>Peace out</i></b>