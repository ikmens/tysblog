---
title: "Peerfeedback moment!"
subtitle:  "Gezellig"
date:  2018-03-26
categories: ["School","Kwartaal3"]
tags: ["Blog","School"]
---
Dus we zijn aan het eind van het kwartaal. Het was een mooi moment om peerfeedback te vragen over hoe ik het heb gedaan als teamlid/captain. Dus dat heb ik gevraagd.
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Ik heb gevraagd over hoe ze het in het team vonden, hoe ze vonden dat ik het heb gedaan, wat ik nog beter kan doen en wat er volgend kwartaal beter kan gaan.
Over het algemeen zeiden ze wat ik had verwacht en hoewel dat fijn is vind ik het ook wel jammer. 
<br />
<br />
Een paar dingen die ze hebben gezegd:
<ul>
<li>Ze vonden het gezellig in het groepje.</li>
<li>Ze vonden dat er niet veel was te doen dit kwartaal</li>
<li>Ik moet soms voor wat meer orde zorgen want soms worden we te snel afgeleid door andere of onszelf</li>
</ul>