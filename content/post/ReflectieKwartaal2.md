---
title: "Terugblik"
subtitle:  "Paard"
date:  2018-01-25
categories: ["Personal progression","Kwartaal2"]
tags: ["Blog","School"]
---
Kwartaal 2, het kwartaal waarin we voor het eerst een "echte" opdracht kregen en waarin 2 teamleden weggingen.
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<b>Wat vond ik van de opdracht</b> 
<br />
Niet echt super leuk. Dit is vooral omdat mijn doelgroep 38+ was. De enige doelgroep waarmee het lastig is om mee te verbinden.
Het kwam door deze leeftijdsverschil van minstens 20 jaar dat er soms moeite was om iets te verzinnen voor het project. 
Voor de rest vond ik het een gaaf ding om te doen. Ik vond het gaaf dat we naar het Paard gingen voor een briefing en dat Fabrique naar school kwam voor de expos.
<br />
<b>Wat vond ik van het groepje?</b>
<br />
We begonnen met ze vijfen (Pascalle, Ivana, Jari, Josephin en Ik). Na een tijd kwam Ivana steeds minder opdagen en daarna Pascalle.
Het werd best wel lastig maar we moesten door werken en dat hebben we gedaan. Ik ben blij dat we het konden afmaken met z'n drieën.
Soms was het lastig omdat iedereen een beetje down werd door de andere groepjes die zo ver waren vergelijken met ons. Sommige dingen hebben we niet kunnen doen zoals de recap.
Maar wat we hebben gedaan vond ik zelf veel beter dan ik had gedaan in kwartaal 1. Overal was ik blij dat we dingen konden uitwerken maar ik weet niet als ik nog een keer in een groepje met hen wil.
Misschien is het door de 2 teamleden die weg gingen maar voor mijn gevoel werkte ik beter in mijn alle eerste groepje.
<br />
