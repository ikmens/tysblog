---
title: "STARRTs geschreven"
subtitle:  "ship of theseus"
date:  2018-01-19
categories: ["Personal progression","Kwartaal2"]
tags: ["Blog","School"]
---
STARRTs geschreven van dit kwartaal. 6 STARRTs waarvan er 1 is van kwartaal 1. 
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Nu nog mijn Nederlands tekst schrijven. 
Daarna nog een toets voor Nederlands, herkansing voor tekening en mijn assessment gesprek.
Voor de Nederlands tekst ga ik schrijven over de [Ship of Theseus](https://en.wikipedia.org/wiki/Ship_of_Theseus) sinds ik dat best wel interessant vind.
<br />
<br />
Korte samenvatting van [Ship of Theseus](https://en.wikipedia.org/wiki/Ship_of_Theseus) : Er word een boot gebruikt om te reizen en tijdens het reizen worden er steeds planken vervangen die oud en zwak worden.
Na een tijd is al het hout in van deze boot vervangen. 

Dan is de vraag nog:<ul style="list-style-type:square">
<li>Is het dezelfde boot</li>
of 
<li>Is het een compleet nieuwe boot.</li>
</ul>
<br />
<br />
Ook kan je nog verder gaan en dan de vraag stellen: 
Als het oude hout word verzameld en een boot van word gemaakt welke boot is dan de originele boot?
<ul style="list-style-type:square">
<li>Is het de boot waarvan het hout is vervangen.</li>
<li>Is het de boot die is gemaakt van het oude hout.</li> 
of
<li>Is geen van de twee de originele boot.</li>
</ul>