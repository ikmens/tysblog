---
title: "So I forgot a week"
subtitle:  "Not okay"
date:  2019-12-05
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
Ik ben dus een week vergeten te bloggen. Hierin heel kort een recap van wat er is gebeurd voor de laastste 5 dagen en vandaag...
<!--more-->

<br />
<br />
<br />
<br />
<h3>Donderdag November 28</h3>
<p>
Ik werkte vandaag in Rotterdam. Vandaag kon ik helaas niet met Michelle afspreken omdat ze op werk moest zijn dus dan maar in me eentje gaan kijken naar podcast dingen. Ook was me plan om naar een aantal dingen van werk te kijken maar het lukte vandaag niet echt dus was wat eerder gestopt. Iets na 3 uur.
</p>
<h3>Vrijdag November 29</h3>
<p>
Op werk vandaag. Mijn stagebegeleidster werkt vandaag thuis dus zo goed als de enige designer. Ben bezig geweest met de criteria update.
</p>
<h3>Maandag December 2</h3>
<p>
Meer werk aan de criteria gebeuren en voorbereiden om een workshop met Michelle te geven morgen.
</p>
<h3>Dinsdag December 3</h3>
<p>
Workshop gegeven met Michellen. Helaas waren en niet zoveel mensen als gehoopt. Het was voornamelijk mensen bekend maken met Figma en Sketch. Super vaag dit! Sommige studenten hadden zoveel moeite ermee en dat vond ik een beetje jammer. Ook met een docente erover gehad dat de meeste studenten pas echt op stage erachter komen wat je allemaal moet doen en waarom. Ook over hoe je ze al eerder bewust ervan kan maken sinds in jaar 3 het al best wel laat is. Niet te laat maar het liefsts wil je ze in jaar 2 of eind jaar 1 ze al een beetje bewust ervan maken.
</p>
<h3>Woensdag December 4</h3>
<p>
Vandaag bezig geweest met een template maken voor sales om te gebruiken. Voelde me best wel oncomfortabel omdat ik nog nooit een template had opgezet voor iets. Gelukkig had een sales intern al iets geprobeerd en ben ik daar maar verder op gegaan. 
Dat heeft me de helft van de dag geduurt maar daarna bezig geweest met prototypes helpen en crtieria update. Ik moet een review gaan doen met de andere designer/front-ender om een beetje input te krijgen omdat mijn begeleidster nog steeds thuis werkt momenteel en ik het minder fijn vind via slack. 
</p>
<h3>Donderdag December 5</h3>
<p>
Rotterdam dag! In de ochtend voornamelijk bezig geweest met het schrijven van een Episode Zero in me eentje omdat Michelle bezig is. Ik weet niet hoe invested zij nog is dus misschien moet ik het gewoon in me eentje gaan doen. 
Ik wil graag nog beginnen wanneer ik nog op stage ben maar ik moet ook binnenkort beginnen aan me stageverslag waar ik totaal geen zin in heb tbh. 
</p>
<br />
<br />
Dat is de recap voor de laatste paar dagen dus. <b><i>Peace Out. Girl Scout!</i></b>