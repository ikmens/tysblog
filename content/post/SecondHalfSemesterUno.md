---
title: "Recap semester 1,0 jaar 2"
subtitle:  "Mogelijk één van de laatste posts."
date:  2019-01-23
categories: ["School","Year 2"]
tags: ["SemesterUno"]
---
Hey gelukkig nieuw jaar. New year New post? 
<!--more--> 

<br />
<br />
<br />
<br />
<br />
Om verder te gaan met de vorige post. Het concept waar ik aan heb gewerkt. <b><i>Oogcontact</b></i>. Met dit concept zijn we verder gegaan en hebben we gecombineerd met een gedeelt van Sico's concept met VR maar inplaats van VR AR a.k.a. Augmented Reality. 
Nu heeft het concept een soort toekomstig beginpunt en een toekomstig pad dat uiteindelijk ervoor zorgt dat de meeste zaken thuis geregeld kunnen worden doormiddel van AR.
De AR bril is momenteel een hout uitgesneden versie gemaakt door Sico. 
<br />
<br />
<img src= "https://i.imgur.com/akWjf77.jpg" width="50%" height="50%"/>
<br />
<br />
Dit samen met een voorbeeld filmpje en voorbeeld schermen van het eyetracking is wat op de expo gaan staan. Hopelijk vind Woonstad het even mooi en interessant als wij het vinden. 
<br />
<br />
<h2>Reflectie Semester</h2>
<br />
<br />
Het was heel anders dan jaar 1 omdat ik nu peercoach ben en omdat er minder begleiding is. En dat is heel fijn want het zorgt ervoor dat wanneer je iets goed maakt dat het dan ook door jezelf komt en niet doordat de docenten je hebben geholpen. 
Dat is nu het geval we hebben iets gemaakt het is best wel gaaf en mensen zijn onder de indruk van wat ze hebben gezien. Sommige dingen konden aan het begin wel wat beter gaan maar zoals elke student geeft als excuus <i>"Het is net vakantie geweest"</i>. 
Het einde ging een heel stuk beter. Iedereen was weer in de flow van CMD en ging zijn gang met het project en keuzevakken. Voor de rest heb ik niet echt veel te zeggen. 
CMD is CMD en het is nog steeds altijd wel lachen om er te zijn en nieuwe dingen te leren. Mogelijk meer blogs aankomend Semester maar kan niks beloven. 
<br />
<br />
<b><i>Peace out. Girl Scout</i></b>