---
title: "Monday restart"
subtitle:  "I don't know any other fruits that start with M besides Melon....Oh mango!"
date:  2019-09-30
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
New week, New chances......
<!--more-->

<br />
<br />
<br />
Hey daar, vandaag was er niet veel aan de hand vooral werken aan mijn project. Buiten dat had ik een momentje met Lily om over van alles te praten. Dus of ik nog wat miste of ik me ergens oncomfortabel voelde hoe mijn onboarding gaat. 
Geen problemen mee als je je dat afvraagt! Maar heel van dat dit soort momentjes te zijn voor als er ooit echt iets is. 
<br />
<br />
Oh ja Hasan's laatste dag bij FBF :( Hij was al een tijdje een stagair hier en heeft best wel wat dingen gedaan dus was zeker interessant om met hem te praten en te zien wat hij had achter gelaten voor ons. 
Buiten dit is er niet veel anders gebeurd. Morgen ga ik naar school voor mijn terugkomdag dus zal ik niet naar werk gaan omdat ik het anders niet zou halen. Dus daarom geen blogpost voor 1 okt. 
<br />
<br />
<b><i>Peace out. Girl Scout!</i></b>