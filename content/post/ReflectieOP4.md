---
title: "Het zit er weer op!"
subtitle:  "Looking back"
date:  2018-06-03
categories: ["School","Kwartaal4"]
tags: ["Blog","School"]
---
Het kwartaal is weer voorbij. Nu is de tijd om terug te kijken en te zeggen hoe ik het vond gaan het gehele jaar. 
Veel geleerd. Veel gave mensen ontmoet. Veel gezien.
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
<h2>Kwartaal 1</h2>
<br />
Het allereerste kwartaal. Tijdens dit kwartaal zat ik in het groepje <i>"Simplicity Matters"</i> met Sem, Gabi, Savannah en Maarten. 
Alles was nieuw en eng. Alles was mogelijk. We moesten iets creëren waardoor nieuwe studenten de studie, elkaar en de stad leren kennen. 
Het was zeker lastig om met iets te komen. We hebben <b>2</b> keer een [Kill your Darling](http://bfy.tw/IQut) gedaan.
We kwamen bijna tijd te kort omdat we zo vaak opnieuw zijn begonnen. Maar het is ons gelukt aan het einde. Toen het af was wist ik dat ik verder wou met deze studie.
Dit liet mij zien hoe leuk, gaaf, interessant, moeilijk, stressend en veel meer de studie is. Ik was verkocht.
<br />
<br />
<h2> Kwartaal 2</h2>
<br />
Tweede kwartaal. In dit kwartaal gingen we te werk met [PAARD](http://bfy.tw/IQv4). Ik zat in het groepje, That 70s Team, samen met Josephin, Jari, Pascalle en Ivana. 
Dit kwartaal was heel interessant. We moesten te werk gaan met een bedrijf buiten [Hogeschool Rotterdam](http://bfy.tw/IQvH).
Hierin kwam ik erachter hoe het is als 40% van je team plotseling stopt. Tijdens dit kwartaal was het supergaaf dat er een mogelijkheid was dat je kon gaan presenteren in het PAARD.
En sommige mensen hebben dat ook gedaan!
<br />
<br />
<h2>Kwartaal 3</h2>
<br />
Op een na laatste kwartaal. Dit kwartaal gingen we te werk met [EMI](http://bfy.tw/IQvP)! In dit kwartaal en het kwartaal erna heb ik het groepje gezeten met Jari, Savannah, Sophie en Meintje.
Tijdens dit kwartaal moesten we iets verzinnen waardoor we de gezondheid moesten verbeteren van studenten tussen 18-25 jaar oud.
We konden van alles doen van app tot evenement verzinnen. Lastig op sommige momenten omdat je echt goed naar jezelf moest kijken en weten wat jou zou aanspreken.
Daarnaast gingen er ook veel mensen weg van de studie. Niet alleen onze klas maar ook andere en dat was jammer. 
<br />
<br />
<h2>Kwartaal 4</h2>
<br />
Het laatste kwartaal van dit jaar. Nog steeds met het groepje van Kw3. Nu was de opdracht iets anders. Nu moesten we iets verzinnen waardoor we jongeren van 18-25 jaar konden stimuleren een gezondere levensstijl aan te nemen.
Dit was lastig zeg. We hadden Afrikaanderwijk. Niet veel jongeren hangen rond in Afrikaanderwijk. Voornamelijk heb ik dan ook maar ouders geïnterviewd tijdens mijn onderzoeks momenten. 
Heel veel geleerd over Afrikaanderwijk. De wijk waar ik dichtbij woonde en niks over wist. Super gave wijk. Er is 2x per week een markt, er zijn creatieve projecten bezig om de wijk te verbeteren, er is een breakdance klas voor basisschool kinderen en [de Botanische tuin](http://bfy.tw/IQvm)!
Ook veel gedaan op school waar ik op dit moment nog steeds mee bezig ben. Mijn keuzevak overtuigend communiceren heeft mij geleerd om beter te pitchen, het project en de studenten hebben mij meer geleerd over Illustrator, de docenten hebben mij ook veel geleerd over creatieve technieken en dergelijke en natuurlijk <b><i>Tafelvoetbal</i></b>.
Als ik iets heb geleerd is het dat op een plek waar vreemden zitten voor een lange tijd en je wilt dat ze met elkaar gaan praten is het enige dat je moet doen is een tafelvoetbal neerzetten rond hun ruimte en de rest gebeurd van zelf.
<br />
<br />
Ik vond het een geweldig jaar en hoop hier nog te zitten voor een extra 3 jaar. Nog meer geweldige mensen leren kennen, interessante dingen leren en geweldige dingen zien.
Dit was is mijn laatste blogpost voor dit schooljaar hopelijk kan ik volgend schooljaar verder gaan met bloggen.
<br /> 
<br /> 
Peace out ^_^