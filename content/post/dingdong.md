---
title: "Culturele DNA"
subtitle:  "Workshop!"
date:  2018-03-14
categories: ["Personal progression","Kwartaal3"]
tags: ["Blog","School","Project"]
---
Ik had een workshop vandaag die over [culturele dna](https://www.youtube.com/watch?v=thTgveMQcKE) ging.
<!--more-->

<br />
<br />
<br />
<br />
<br />
<br />
Hierin hebben we gekeken wat onze persoonlijke Culturele DNA is.
Ik ben meer van boven naar beneden: Achieved, Direct communication, Animated/Rational, Relationship, Low power distance, individualisme.
<br />
<img src="https://i.imgur.com/TktWQD7.jpg" alt="lifestylewords" width="500" height="600">