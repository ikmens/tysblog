---
title: "No hard feelings"
subtitle:  "Only soft"
date:  2018-09-19
categories: ["School","Year 2"]
tags: ["SemesterUno"]
---
Voor het nieuwe team is ook een net mooi nieuw logo nodig. <strike>Dit logo heb ik totaal niet uit verveling gemaakt</strike>
<!--more-->

<br />
<br />
<br />
<br />
<br />
Natuurlijk voor elke geweldige creatie is er inspiratie. Deze inspiratie heb ik opgedaan via [Pinterest](https://www.pinterest.com). Ik heb vooral gekeken naar logo's met een [serif font](https://www.google.nl/search?q=serif+font&oq=serif+font&aqs=chrome..69i57j69i59j69i60l3j0.2185j0j7&sourceid=chrome&ie=UTF-8). Mijn keuze hiervoor was omdat ik voor een professionele stijl ging omdat wij in onze afspraken ook hadden afgesproken elkaar zo goed mogelijk te helpen en dingen te zeggen als ze fout gaan oftewel "No hard feelings"
<br />
<br />
<img src= "https://i.imgur.com/buDinnK.png" width="50%" height="50%"/>
<br />
<br />
Als volgt was ik schetsen gaan maken. Helaas heb ik de blaadjes ervoor niet meer. Maar van deze schetsen heb ik er 3 gedigitaliseerd. Deze 3 hadden allemaal wel wat dat ik mooi vond.
<br />
<br />
<img src= "https://i.imgur.com/EMn7G1N.png" width="50%" height="50%"/>
<br />
<br />
Vanuit deze 3 ben ik uiteindelijk op 1 design gebleven. Dit design vond ik het mooist en had allerlei dingen van de andere gecombineerd.
<br />
<br />
<img src="https://i.imgur.com/ODGH0KB.png" width="50%" height="50%"/>
<br />
<br />
Uiteindelijk had ik nog de team naam eronder gezet om het af te maken. Ook had ik nog een alternatief gemaakt erop die inplaats van een blokje, een lijn aan beide kanten. 
<br />
<br />
<img src="https://i.imgur.com/JUYsD41.png" width="50%" height="50%"/>
<br />
<br />
<img src= "https://i.imgur.com/emN0bMZ.png" width="50%" height="50%"/>


