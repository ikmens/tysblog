---
title: "Even koekeloeren"
subtitle: "2e jaars kwam langs voor een interview"
date:  2019-11-04
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
Dus ik ben geïnterviewd door een 2e jaars wat leuk was, sprintplanning, feedback op prototype en feedback geven...
<!--more-->

<br />
<br />
<br />
Dus ja een drukke dag vandaag. Laat ik maar bij het begin beginnen: 
<br />
<h3>Sprint planning</h3>
<br />
Deze sprint planning is iets anders dan de andere waar ik aan mee heb gedaan omdat dit de laatste is van het seizoen! Deze duurt 3 weken aka 120 uur dus ik moest wat meer inplannen. 
Om het kort te zetten ik heb ingepland om deze week mijn project af te maken en te beginnen aan een nieuw, korter project en om dit kortere project af te maken in de laatste 2 weken. 
Dussss laten we het hopen. Maar dit project gaat af deze planning want ben er wel blij mee. 
<br />
<h3>Interview</h3>
<br />
Dus Dylan was langsgekomen voor een interview. Hetzelfde dat ik vorig jaar had gedaan toen ik langs een 3e jaars ging om te kijken hoe zijn stage was. Hij wat vragen over hoe ik het bedrijf heb gevonden, hoe lang het me duurde, met hoeveel bedrijven ik heb gesproken en als ik het eng vond. 
Allemaal hele normale dingen om te vragen want natuurlijk is het wel een beetje eng om plotseling van school fulltime te gaan werken in een of ander random bedrijf. Maar het is ook harstikke leerzaam en gaaf en alles. 
<br />
<h3>Feedback</h3>
<br />
Last but not least feedback krijgen en geven. Voor 2 verschillende dingen. Ik heb feedback gekregen voor me prototype en op me werkhouding/whatever van de laatste 6 weken en ik heb ook feedback gegeven op mensen en hun werkhouding etc. 
<br />
<br />
Dus eerst prototype feedback. Ik kreeg wat kleine dingen zoals inconsistency of kleuren waren fout maar ook feedback op features die ik was vergeten of om verduidelijking te geven over waarom ik dingen heb aangepast. 
En dat was wel gaaf. Op school hebben we natuurlijk ook geleerd dat je altijd een reden moet hebben voor je aanpassingen en dat soort dingen maar je weet nooit hoe dat gaat in de praktijk en nu kreeg ik letterlijk de vraag waarom heb ik dit er zo laten uitzien. 
Daarop gaf ik dan mijn reden ervoor en ze was het er mee eens maar ze wou zien of ik dat had gedaan om iets te veranderen of voor een geldige reden en dat is wel gaaf. 
<br />
<br />
Vervolgens feedback geven en nemen op werkhouding. Ook leuk. Dit doen ze dus elk half jaar(?) en worden vervolgd met een <u>"Growth talk"</u>. Omdat dit gehele gebeuren gefocust is op hoe je je kan verbeteren en hoe je dat wilt gaan doen in het volgende half jaar. 
Ik heb dus mijn eigen "Growth talk" woensdag en ben beniewd hoe het gaat en of ik zoiets ook kan gaan doen op school. En dat was het wel weer voor vandaag dan...
<br />
<br />
<b><i>Peace Out. Girl Scout!</i></b>