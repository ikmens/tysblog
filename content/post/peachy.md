---
title: "Let's start working"
subtitle:  "I'm gonna need to think of some fruit puns"
date:  2019-09-24
categories: ["School","Year 3"]
tags: ["Year3, Stage"]
---
First real day. Planning maken en project aangewezen gekregen...
<!--more-->

<br />
<br />
<br />
<br />
Oke dus de dag begon met een soort meeting/brainstorm over de volgende stap in een groot project waar ze mee bezig zijn en daar zat ik bij en heb ik zelfs aan meegedragen! 
Vervolgens was ik aan de gang gegaan met het lezen over hun projecten en het programma waar ze mee werken. Dit nam de meeste tijd voor me in vandaag maar zorgde er wel voor dat ik een beetje up to date was met waar alles over ging. 
Na dat kreeg ik mijn mijn project aangewezen! <i>voor dit kwartaal.</i> Het is een iets kleiner projectje maar nog steeds interessant. Ik heb wat userstories/feedback gekregen over een bepaalde feature in het Feedbackfruits programma/toolset/etc. en ik ga dan kijken hoe we dat kunnen verbeteren. 
Ik moest er dan wel op letten dat het overeenkomt met andere sets van het Feedbackfruits ding zodat gebruikers het herkennen en niet iets nieuws hoeven te leren. A.k.a. make a new mental model for the task. 
<br />
<br />
Aan het einde van de dag was ik bij mijn eerste "Tuesday update". In deze update vertellen ze allemaal hoe het gaat met het bedrijf, laten ze cijfers zien en nog veel meer. Dit was echt heel gaaf om mee te maken omdat me ook een beetje laat zien waar ze naar toe willen gaan en waar ze nu zijn. 
En als allerlaatst was er team dinner! Elke dinsdag avond kan je ervoor kiezen om te avond eten met de rest van het team en een beetje lachen en praten. Super nice dit omdat het ervoor zorgt dat je je gelijk bij het team voelt. Ik ben hier pas 2 dagen maar ik heb zoveel meegemaakt dat ik niet kan wachten voor de rest. 
<br />
<br />
<b><i>Peace out. Girl Scout!</i></b>