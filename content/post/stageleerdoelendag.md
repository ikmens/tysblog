---
title: "Prepping"
subtitle:  "Stageleerdoelen"
date:  2019-05-09
categories: ["School","Year 2"]
tags: ["SemesterDos"]
---
Stageleerdoelen wat doe je er mee? Naja best wel wat eigenlijk...
<!--more-->

<br />
Donderdag was geen project dag voor ons (mijn team en ik). We, naja iedereen van jaar 2 CMD, zijn bezig geweest met onze stageleerdoelen. Dit zijn leerdoelen die je hoopt te behalen tijdens je stage. 
Met nadruk op hoopt. Er is een mogelijkheid dat je het niet heb geleerd op je stage of niet tot het extent dat jij had gehoopt maar dan kan je nog steeds erover schrijven waarom je het niet heb gehaald en of dat komt door jezelf, het bedrijf of dat je de lat te hoog had gelegd. 
Mijn stageleerdoelen zijn nog niet af bij het schrijven van deze blogpost. Ik ben pas bij leerdoel 3 en we moeten er 6 hebben om op stage te gaan. Dus dat word nog ff bikkelen binnenkort. 
<br />
<br />
<b><i>Peace out! Girl Scout</i></b>